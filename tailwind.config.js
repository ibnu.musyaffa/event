const { colors } = require("tailwindcss/defaultTheme");

module.exports = {
  corePlugins: {
    preflight: false
  },
  theme: {
    extend: {
      colors: {
        orange: {
          ...colors.orange,
          "400": "#fcb040"
        },
        teal: {
          ...colors.teal,
          "500": "#18B0B3"
        }
      }
    }
  },
  variants: { backgroundColor: ["group-hover","hover"] },
  plugins: []
};
