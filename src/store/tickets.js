import { createSlice } from "@reduxjs/toolkit";

const tickets = createSlice({
  name: "tickets",
  initialState: [],
  reducers: {
    syncTicket: (state, action) => {
      //remove ticket if qty == 0
      if (action.payload.qty == 0) {
        let filteredState = state.filter(item => {
          return item.id != action.payload.id;
        });
        return filteredState;
      }

      //check is exist
      let isExist = state.filter(item => {
        return item.id == action.payload.id;
      });

      if (isExist.length > 0) {
        let newState = state.map(item => {
          if (item.id == action.payload.id) {
            item = action.payload;
          }
          return item;
        });

        return newState;
      }

      state.push(action.payload);
    },
    truncateTickets: (state, action) => {
      state = [];
      return state;
    }
  }
});
export const { syncTicket, truncateTickets } = tickets.actions;
export default tickets.reducer;
