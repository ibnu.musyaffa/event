import { createSlice } from "@reduxjs/toolkit";

const event = createSlice({
  name: "event",
  initialState: null,
  reducers: {
    setEvent: (state, action) => {
      state = action.payload;
      return state;
    },
    truncateEvent: (state, action) => {
      state = null;
      return state;
    }
  }
});
export const { setEvent, truncateEvent } = event.actions;
export default event.reducer;
