import { combineReducers } from "@reduxjs/toolkit";
import event from "./event";
import tickets from "./tickets";
import user from "./user";

const rootReducer = combineReducers({
  event: event,
  tickets: tickets,
  user
});

export default rootReducer;
