import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER
} from "redux-persist";
import expireReducer from "redux-persist-expire";
import storage from "redux-persist/lib/storage"; // defaults to localStorage for web
import rootReducer from "./reducer";
import history from "../helper/history";

let username = history.location.pathname.split("/")[1];


const persistedReducer = persistReducer(
  {
    key: username ? username : "????",
    storage,
    // whitelist: ['user'],
    transforms: [
      expireReducer("cart", {
        // persistedAtKey: "loadedAt",
        expireSeconds: 2,
        expiredState: [],
        persistedAtKey: 'loadedAt',
      })
    ]
  },
  rootReducer
);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: getDefaultMiddleware({
    serializableCheck: {
      ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER]
    }
  })
});
export const persistor = persistStore(store);
