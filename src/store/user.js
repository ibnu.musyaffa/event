import { createSlice } from "@reduxjs/toolkit";

const user = createSlice({
  name: "cart",
  initialState: {},
  reducers: {
    setUser: (state, action) => {
      state = action.payload
      return state;
    },
    deleteUser: (state, action) => {
      return {}
    }
  }
});
export const {
  setUser,
  deleteUser
} = user.actions;
export default user.reducer;
