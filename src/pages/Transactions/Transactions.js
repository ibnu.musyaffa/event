import React, { Component } from "react";
import InfiniteScroll from "react-infinite-scroller";
import { Modal, List, Spin, Checkbox, Form, Empty, Select, Input } from "antd";
import { withRouter } from "react-router-dom";
import Navigation from "../../components/Navigation";
import TransactionCard from "./TransactionCard";
import createHeaders from "../../helper/headers";
import axios from "axios";
import url from "../../config/url";

export class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      loading: false,
      hasMore: true,
      currentPagination: 0
    };
    this.handleMore = this.handleMore.bind(this);
    this.toDetail = this.toDetail.bind(this);
  }

  componentDidMount() {
    this.handleMore();
  }

  toDetail(id, token,status) {
    console.log(token);
    let url;
    if (status === "PAID" || status === "CONFIRMED") {
      url = `/${this.props.match.params.username}/transaction-detail/${id}/${token}`;
    } else {
      url = `/${this.props.match.params.username}/checkout-detail/${id}/${token}`;
    }

    this.props.history.push(url);
  }

  async handleMore() {
    try {
      this.setState({
        loading: true
      });

      let headers = createHeaders();
      let response = await axios.get(`${url}api/event/transaction`, {
        headers,
        params: {
          page: this.state.currentPagination + 1
        }
      });
      response = response.data;
      console.log(response);

      if (response.data.data.length > 0) {
        this.setState({
          loading: false,
          data: [...this.state.data, ...response.data.data],
          currentPagination: this.state.currentPagination + 1
        });
      } else {
        this.setState({
          loading: false,
          hasMore: false
        });
      }
    } catch (error) {
      this.setState({
        loading: false
      });
    }
  }

  render() {
    let isEmpty = this.state.data.length == 0 && !this.state.loading;
    return (
      <div className="w-full">
        <Navigation withBack noCart title="Transaksi Saya"></Navigation>
        <div className="flex justify-center items-center h-full flex-col mb-8">
          <div className="flex flex-col px-5 w-full md:w-1/3 lg:w-1/3 xl:w-1/3 pt-5">
            <div className="">
              <InfiniteScroll
                initialLoad={false}
                pageStart={0}
                loadMore={this.handleMore}
                hasMore={!this.state.loading && this.state.hasMore}
                useWindow={true}
              >
                {this.state.data.map(item => (
                  <TransactionCard
                    onClick={() =>
                      this.toDetail(item.id,item.token, item.status_pembayaran)
                    }
                    key={item.id}
                    data={item}
                  ></TransactionCard>
                ))}

                {this.state.loading && this.state.hasMore && (
                  <div className="flex justify-center items-center h-12">
                    <Spin />
                  </div>
                )}
                {isEmpty && (
                  <div className="h-64  flex justify-center items-center">
                    <Empty description="Transaksi kosong"></Empty>
                  </div>
                )}
              </InfiniteScroll>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Login);
