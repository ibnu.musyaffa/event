import React, { Component } from "react";
import url from "../../config/url";
import format from "../../helper/format";
import { Icon } from "antd";
function Red(props) {
  return (
    <span className=" p-1 bg-red-200 text-xs  rounded-sm text-red-800 px-1">
      {props.children}
    </span>
  );
}

function Green(props) {
  return (
    <span className=" p-1 bg-green-200 text-xs  rounded-sm text-green-800 px-1">
      {props.children}
    </span>
  );
}

function Blue(props) {
  return (
    <span className=" p-1 bg-blue-200 text-xs  rounded-sm text-blue-800 px-1">
      {props.children}
    </span>
  );
}

function Gray(props) {
  return (
    <span className=" p-1 bg-gray-200 text-xs  rounded-sm text-gray-800 px-1">
      {props.children}
    </span>
  );
}

function Yellow(props) {
  return (
    <span className=" p-1 bg-yellow-200 text-xs  rounded-sm text-yellow-800 px-1">
      {props.children}
    </span>
  );
}

function TransactionCard(props) {
  let t = props.data;
  return (
    <div
      onClick={props.onClick}
      className="group border-solid border-gray-300 border rounded-sm  
        mb-5 cursor-pointer h-auto  p-3 flex hover:bg-blue-100"
    >
      <div className="mr-3" style={{ height: 100, width: 100 }}>
        <img
          style={{ height: 100 }}
          className="rounded"
          src={`${url}${t.event.image_cover_url}`}
        ></img>
      </div>
      <div className="w-full">
        <div className="mb-2 flex justify-between">
          {t.status_pembayaran == "UNPAID" && <Yellow>{t.status_pembayaran}</Yellow>}
          {t.status_pembayaran == "CONFIRMED" && <Blue>{t.status_pembayaran}</Blue>}
          {t.status_pembayaran == "EXPIRED" && <Red>{t.status_pembayaran}</Red>}
          {t.status_pembayaran == "PAID" && <Green>{t.status_pembayaran}</Green>}
          <div>{t.created_at}</div>
        </div>
        <div className="font-semibold py-2 bg-gray-100 px-2 mb-2 rounded">
          {t.event.nama_event}
        </div>
        {/* <div className="border-b border-solid border-r-0 border-l-0 border-t-0 border-gray-400 my-2"></div> */}
        <div className="flex justify-between ">
          <div className="flex items-center ">
            <Icon
              className="text-lg"
              style={{ color: "#1c9fad", marginRight: 7 }}
              type="tag"
            />
            <div>{t.tickets.length} Tiket</div>
          </div>
          <div className="font-semibold">Rp {format(t.total)}</div>
        </div>
      </div>
    </div>
  );
}

export default TransactionCard;
