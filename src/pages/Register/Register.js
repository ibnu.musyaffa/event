import React, { Component } from "react";
import { Form, Icon, Input, Checkbox, message } from "antd";
import Button from "../../components/ButtonTeal";
import url from "../../config/url";
import axios from "axios";
import FormItem from "../../components/FormItem";
import { withRouter } from "react-router-dom";
import Navigation from "../../components/Navigation";
import { Formik } from "formik";
import * as Yup from "yup";

const RegisterShape = Yup.object().shape({
  name: Yup.string().required("Nama wajib diisi"),
  username: Yup.string().required("Username wajib diisi"),
  email: Yup.string()
    .required("Email wajib diisi")
    .email("Email tidak valid"),
  phone_number: Yup.string().required("Nomor handphone wajib diisi"),
  password: Yup.string()
    .required("Password wajib diisi")
    .min(6, "Password minimal 6 karakter"),
  password_confirmation: Yup.string().required(
    "Konfirmasi Password wajib diisi"
  )
});

export class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      initialValues: {
        name: "",
        username: "",
        email: "",
        phone_number: "",
        password: "",
        password_confirmation: ""
      }
    };
    this.toRegister = this.toRegister.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  toRegister() {
    let url = `/${this.props.match.params.username}/register`;
    this.props.history.push(url);
  }

  async onSubmit(values, actions) {
    try {
      let res = await axios.post(`${url}api/auth/register`, values);
      console.log(res.data.code);
      if (res.data.code == "00") {
        message.success(res.data.info);
        let url = `/${this.props.match.params.username}/register-verifikasi`;
        this.props.history.push(url, { email: values.email });
      } else {
        message.error(res.data.info);
      }
    } catch (err) {}
  }
  render() {
    return (
      <div className="w-full">
        <Navigation withBack noCart title="Daftar"></Navigation>
        <div className="flex justify-center items-center h-full flex-col mb-8">
          <div className="flex flex-col px-5 w-full md:w-1/3 lg:w-1/3 xl:w-1/3 pt-5">
            <div className="mx-3 md:mx-0">
              <Formik
                initialValues={this.state.initialValues}
                enableReinitialize
                validationSchema={RegisterShape}
                onSubmit={this.onSubmit}
                validate={values => {
                  const errors = {};
                  if (values.password !== values.password_confirmation) {
                    errors.password_confirmation =
                      "Konfirmasi password tidak benar";
                  }
                  return errors;
                }}
              >
                {({
                  isSubmitting,
                  handleSubmit,
                  handleBlur,
                  handleChange,
                  errors,
                  touched,
                  values,
                  setFieldValue,
                  setFieldTouched
                }) => (
                  <Form onSubmit={handleSubmit}>
                    <FormItem
                      error={errors.name}
                      touched={touched.name}
                      label="Nama Lengkap"
                      marginBottom="0.1rem"
                    >
                      <Input
                        name="name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.name}
                        placeholder="Nama Lengkap"
                      />
                    </FormItem>
                    <FormItem
                      error={errors.username}
                      touched={touched.username}
                      label="Username"
                      marginBottom="0.1rem"
                    >
                      <Input
                        name="username"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.username}
                        addonBefore="berbagi.link/"
                        type="text"
                        placeholder="Username"
                      />
                    </FormItem>
                    <FormItem
                      error={errors.email}
                      touched={touched.email}
                      label="Email"
                      marginBottom="0.1rem"
                    >
                      <Input
                        name="email"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.email}
                        type="text"
                        placeholder="Email"
                      />
                    </FormItem>
                    <FormItem
                      error={errors.phone_number}
                      touched={touched.phone_number}
                      label="Nomor Handphone"
                      marginBottom="0.1rem"
                    >
                      <Input
                        name="phone_number"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.phone_number}
                        type="text"
                        placeholder="Nomor Handphone"
                      />
                    </FormItem>
                    <FormItem
                      error={errors.password}
                      touched={touched.password}
                      label="Password"
                      marginBottom="0.1rem"
                    >
                      <Input.Password
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.password}
                        name="password"
                        type="text"
                        placeholder="Password"
                      />
                    </FormItem>
                    <FormItem
                      error={errors.password_confirmation}
                      touched={touched.password_confirmation}
                      label="Konfirmasi Password"
                      marginBottom="20"
                    >
                      <Input.Password
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.password_confirmation}
                        name="password_confirmation"
                        type="text"
                        placeholder="Password"
                      />
                    </FormItem>
                    <FormItem>
                      <Button
                        type="primary"
                        htmlType="submit"
                        block
                        loading={isSubmitting}
                      >
                        Submit
                      </Button>
                    </FormItem>
                  </Form>
                )}
              </Formik>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Login);
