import React, { Component } from "react";
import { Button, message } from "antd";
import { CopyToClipboard } from "react-copy-to-clipboard";
import url from "../../config/url";
export class Bank extends Component {
  copied() {
    message.success("Berhasil dicopy");
  }
  render() {
    return (
      <div className="flex mt-3 border border-solid rounded border-gray-400 p-3 hover:bg-gray-100">
        <div className="flex items-center">
          <div>
            <img
              alt=""
              className="object-scale-down"
              style={{ height: 50, width: 70 }}
              src={`${url}${this.props.item.type.img}`}
            ></img>
          </div>
        </div>
        <div className="text-base pl-5 flex justify-between w-full">
          <div className="">
            <div className="font-semibold text-sm">
              {this.props.item.type.nama} - {this.props.item.nomor_rekening}
            </div>
            <div className="text-sm">{this.props.item.nama_rekening}</div>
          </div>

          <div className="flex items-center">
            <CopyToClipboard
              text={this.props.item.nomor_rekening}
              onCopy={this.props.onCopy}
            >
              <Button value="small" icon="copy" size="small"></Button>
            </CopyToClipboard>
          </div>
        </div>
      </div>
    );
  }
}

export default Bank;
