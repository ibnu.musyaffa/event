import React, { Component } from "react";
import Bank from "./Bank";
import { Button, message, Spin, Icon } from "antd";
import ButtonCustom from "../../components/Button";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { withRouter } from "react-router-dom";
import format from "../../helper/format";
import ProductItem from "../../components/ProductItem";
import CustomerInfo from "../../components/CustomerInfo";
import axios from "axios";
import url from "../../config/url";
import { Helmet } from "react-helmet";
import NavigationLight from "../../components/NavigationLight";
import EventSmall from "../../components/EventSmall";

function Pembayaran(props) {
  return (
    <React.Fragment>
      <div className="font-semibold text-lg mb-3 mt-4">Pembayaran</div>
      <div className="">
        <div className="mt-3 flex justify-between items-center py-2 px-1">
          <div className="text-gray-700 font-medium">Total Harga Tiket</div>
          <div className="text-gray-900">
            Rp {format(props.transaction.total - props.transaction.kode_unik)}
          </div>
        </div>
        <div className="flex justify-between bg-gray-100 py-2 px-1">
          <div className="text-gray-700 font-medium">Kode unik</div>
          <div className="text-gray-900">
            Rp {format(props.transaction.kode_unik)}
          </div>
        </div>
        <div className="flex justify-between items-center py-2 px-1">
          <div className="text-gray-700 font-medium">Total Pembayaran</div>
          <div className="text-gray-900">
            Rp {format(props.transaction.total)}
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

function PaymentMethodList(props) {
  return (
    <React.Fragment>
      <div className="font-semibold text-lg mb-3 mt-5 mb-4">
        Informasi Rekening Tujuan
      </div>
      {props.bankList.map((item) => (
        <Bank key={item.id} onCopy={props.onCopy} item={item}></Bank>
      ))}
    </React.Fragment>
  );
}

export class CheckoutDetail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      bankList: [],
      isFetching: true,
      transaction: {},
      showDetail: true,
      status: "fetching",
    };
    this.toConfimation = this.toConfimation.bind(this);
    this.toggleShow = this.toggleShow.bind(this);
  }

  toggleShow() {
    this.setState({
      showDetail: !this.state.showDetail,
    });
  }

  componentDidMount() {
    this.fetchTransaction();
    this.fetchBanks();
  }

  async fetchTransaction() {
    try {
      let username = this.props.match.params.username;
      let id = this.props.match.params.id;
      let token = this.props.match.params.token;
      let res = await axios.get(`${url}api/event/transaction/${id}/${token}`);

      let status = res.data.data.status_pembayaran;
      if (status === "PAID" || status === "CONFIRMED") {
        this.props.history.push(
          `/${username}/transaction-detail/${id}/${token}`
        );
      }
      this.setState({
        status: "fetched",
        transaction: res.data.data,
      });
    } catch (error) {}
  }

  async fetchBanks() {
    try {
      let username = this.props.match.params.username;
      let res = await axios.get(`${url}api/payment-method/${username}/event`);
      console.log(res);
      this.setState({
        bankList: res.data.data,
      });
    } catch (error) {}
  }

  copied() {
    message.success("Berhasil dicopy");
  }

  toConfimation() {
    let id = this.props.match.params.id;
    let token = this.props.match.params.token;
    let username = this.props.match.params.username;

    let url = `/${username}/konfirmasi-pembayaran/${id}/${token}`;
    this.props.history.push(url);
  }
  render() {
    if (this.state.status == "fetching") {
      return (
        <div>
          <Helmet>
            <title>Detail Checkout - {this.props.match.params.username}</title>
          </Helmet>
          <div className="flex justify-center items-center h-64">
            <Spin size="large" />
          </div>
        </div>
      );
    }
    return (
      <div>
        <Helmet>
          <title>Detail Checkout - {this.props.match.params.username}</title>
        </Helmet>
        <NavigationLight
          onClick={() => this.props.history.goBack()}
          title="Detail Checkout"
        ></NavigationLight>
        <div className="m-5 flex justify-center">
          <div className="w-full sm:w-2/3 md:w-2/5 lg:w-1/3">
            <div className="flex justify-center flex-col items-center  relative z-20 ">
              <div className=" font-bold text-xl">Terima Kasih</div>
              <div>Silahkan selesaikan pembayaran sebagai berikut</div>
            </div>
            <div className="font-semibold text-lg mb-3 mt-4">
              Informasi Pesanan
            </div>
            <CustomerInfo transaction={this.state.transaction}></CustomerInfo>
            {this.state.transaction.status_pembayaran == "EXPIRED" && (
              <div className="bg-red-200 mt-4 flex justify-center items-center h-16 text-red-800 rounded text-base">
                Transaksi sudah kadaluarsa
              </div>
            )}

            <div className="pt-5">
              {this.state.showDetail && (
                <React.Fragment>
                  <EventSmall event={this.state.transaction.event}></EventSmall>

                  <Pembayaran transaction={this.state.transaction}></Pembayaran>
                </React.Fragment>
              )}

              {this.state.transaction.status_pembayaran == "UNPAID" && (
                <React.Fragment>
                  <div className="mb-5 mt-5">
                    <div className="text-center text-sm">
                      Transfer pembayaran anda sebelum tanggal
                    </div>
                    <div className="text-gray-800 text-center mt-2 text-lg font-semibold">
                      {this.state.transaction.payment_expired_date}
                    </div>
                  </div>

                  <div className="bg-green-100 p-5 rounded">
                    <div className="text-gray-900  text-center text-sm">
                      Jumlah yang harus dibayar
                    </div>
                    <div className="flex justify-center items-center relative mt-2 mb-2">
                      <div className="text-gray-800 font-semibold text-lg flex">
                        <div className="mr-3">
                          Rp {format(this.state.transaction.total)}
                        </div>
                        <div>
                          <CopyToClipboard
                            text={this.state.transaction.total}
                            onCopy={this.copied}
                          >
                            <Button
                              value="small"
                              icon="copy"
                              size="small"
                            ></Button>
                          </CopyToClipboard>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="bg-red-100 text-sm p-3 mt-5 rounded border">
                    <div className="text-red-700 text-center">
                      Mohon transfer sesuai jumlah yang tertera
                    </div>
                    <div className="text-red-700 text-center">
                      Termasuk 3 digit terakhir
                    </div>
                  </div>
                </React.Fragment>
              )}

              {this.state.transaction.status_pembayaran == "UNPAID" && (
                <React.Fragment>
                  <PaymentMethodList
                    bankList={this.state.bankList}
                    onCopy={this.copied}
                  ></PaymentMethodList>

                  <div className="mt-5 bg-yellow-200 p-3 text-center">
                    Pastikan kamu bertransaksi secara baik dan benar, semua
                    transaksi yang terjadi diluar tanggung jawab{" "}
                    <b>Berbagi.link</b> Silahkan Baca{" "}
                    <a target="_blank" href="https://berbagi.link/terms">
                      syarat dan ketentuan
                    </a>{" "}
                    untuk info selengkapnya
                  </div>

                  <div className="mt-8">
                    <ButtonCustom
                      type="primary"
                      block
                      size="large"
                      onClick={this.toConfimation}
                    >
                      Konfirmasi pembayaran
                    </ButtonCustom>
                  </div>
                </React.Fragment>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(CheckoutDetail);
