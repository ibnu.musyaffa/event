import React, { Component } from "react";
import { Form, Icon, Input, Checkbox, message } from "antd";
import Button from "../../components/ButtonTeal";
import url from "../../config/url";
import axios from "axios";
import FormItem from "../../components/FormItem";
import { withRouter } from "react-router-dom";
import Navigation from "../../components/Navigation";
import { Formik } from "formik";
import * as Yup from "yup";

const RegisterShape = Yup.object().shape({
  password: Yup.string()
    .required("Password wajib diisi")
    .min(8, "Password minimal 8 karakter"),
  password_confirmation: Yup.string().required(
    "Konfirmasi Password wajib diisi"
  )
});

export class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      initialValues: {
        password: "",
        password_confirmation: ""
      }
    };

    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    let user = this.props.history.location.state?.user;
    if (!user) {
      let url = `/${this.props.match.params.username}/login`;
      this.props.history.push(url);
    }
  }

  async onSubmit(values, actions) {
    try {
      let res = await axios.post(`${url}api/auth/forgot-password/reset`, {
        ...values,
        ...this.props.history.location.state
      });
      console.log(res.data.code);
      if (res.data.code == "00") {
        message.success(res.data.info);
        let url = `/${this.props.match.params.username}/login`;
        this.props.history.push(url);
      } else {
        message.error(res.data.info);
      }
    } catch (err) {}
  }
  render() {
    return (
      <div className="w-full">
        <Navigation withBack noCart title="Reset Password"></Navigation>
        <div className="flex justify-center items-center h-full flex-col mb-8">
          <div className="flex flex-col px-5 w-full md:w-1/3 lg:w-1/3 xl:w-1/3 pt-5">
            <div className="mx-3 md:mx-0">
              <Formik
                initialValues={this.state.initialValues}
                enableReinitialize
                validationSchema={RegisterShape}
                onSubmit={this.onSubmit}
                validate={values => {
                  const errors = {};
                  if (values.password !== values.password_confirmation) {
                    errors.password_confirmation =
                      "Konfirmasi password tidak benar";
                  }
                  return errors;
                }}
              >
                {({
                  isSubmitting,
                  handleSubmit,
                  handleBlur,
                  handleChange,
                  errors,
                  touched,
                  values,
                  setFieldValue,
                  setFieldTouched
                }) => (
                  <Form onSubmit={handleSubmit}>
                    <FormItem
                      error={errors.password}
                      touched={touched.password}
                      label="Password Baru"
                      marginBottom="0.1rem"
                    >
                      <Input.Password
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.password}
                        name="password"
                        type="text"
                        placeholder="Password"
                      />
                    </FormItem>
                    <FormItem
                      error={errors.password_confirmation}
                      touched={touched.password_confirmation}
                      label="Konfirmasi Password Baru"
                      marginBottom="20"
                    >
                      <Input.Password
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.password_confirmation}
                        name="password_confirmation"
                        type="text"
                        placeholder="Password"
                      />
                    </FormItem>
                    <FormItem>
                      <Button
                        type="primary"
                        htmlType="submit"
                        block
                        loading={isSubmitting}
                      >
                        Submit
                      </Button>
                    </FormItem>
                  </Form>
                )}
              </Formik>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Login);
