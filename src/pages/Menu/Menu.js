import React, { Component } from "react";
import { Form, Icon, Input, Button, Checkbox, Modal, message } from "antd";
import url from "../../config/url";
import FormItem from "../../components/FormItem";
import { withRouter } from "react-router-dom";
import Navigation from "../../components/Navigation";
import { setUser } from "../../store/user";
import { connect } from "react-redux";

const { confirm } = Modal;

function BoxMenu(props) {
  return (
    <div
      onClick={props.onClick}
      className="border-solid border-gray-300 border mb-3 rounded-sm py-5 px-4 hover:bg-gray-100 cursor-pointer flex "
    >
      <div className="mr-3">
        <Icon
          type={props.icon}
          style={{
            fontSize: "1.25rem"
            // color: props.iconColor ? props.iconColor : "#757575"
          }}
        ></Icon>
      </div>
      <div>{props.title}</div>
    </div>
  );
}

function BoxLogout(props) {
  return (
    <div
      onClick={props.onClick}
      className="border-solid border-gray-300 border mb-3 rounded-sm py-5 px-4 hover:bg-red-100 cursor-pointer flex "
    >
      <div className="mr-3">
        <Icon
          type={props.icon}
          style={{
            fontSize: "1.25rem",
            color: "#f56565"
          }}
        ></Icon>
      </div>
      <div className="text-red-500">{props.title}</div>
    </div>
  );
}

export class Menu extends Component {
  constructor(props) {
    super(props);

    this.state = {};
    this.toLogout = this.toLogout.bind(this);
    this.toTransaction = this.toTransaction.bind(this);
    this.clear = this.clear.bind(this);
    this.toProfile = this.toProfile.bind(this);
    this.toEditPassword = this.toEditPassword.bind(this);
  }

  toLogout() {
    confirm({
      title: "Konfirmasi",
      content: "Apakah anda yakin ?",
      onOk: this.clear,
      onCancel() {
        console.log("Cancel");
      }
    });
  }

  clear() {
    localStorage.clear();
    let url = `/${this.props.match.params.username}`;
    this.props.setUser({});
    this.props.history.push(url);
    message.success("Logout berhasil");
  }

  toTransaction() {
    let url = `/${this.props.match.params.username}/transactions`;
    this.props.history.push(url);
  }

  toProfile() {
    let url = `/${this.props.match.params.username}/edit-profile`;
    this.props.history.push(url);
  }

  toEditPassword() {
    let url = `/${this.props.match.params.username}/edit-password`;
    this.props.history.push(url);
  }

  render() {
    return (
      <div className="w-full">
        <Navigation withBack noCart title="Menu"></Navigation>
        <div className="flex justify-center items-center h-full flex-col mb-8">
          <div className="flex flex-col px-5 w-full md:w-1/3 lg:w-1/3 xl:w-1/3 pt-5">
            <div className="md:mx-0">
              <BoxMenu
                onClick={this.toTransaction}
                title="Transaksi Saya"
                icon="transaction"
              ></BoxMenu>
              <BoxMenu
                onClick={this.toProfile}
                title="Profile"
                icon="user"
              ></BoxMenu>
              <BoxMenu
                onClick={this.toEditPassword}
                title="Ganti Password"
                icon="lock"
              ></BoxMenu>

              <BoxLogout
                onClick={this.toLogout}
                title="Logout"
                icon="logout"
                iconColor="red"
              ></BoxLogout>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user
});

const mapDispatchToProps = dispatch => ({
  setUser: user => dispatch(setUser(user))
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Menu));
