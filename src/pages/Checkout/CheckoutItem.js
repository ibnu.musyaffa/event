import React, { Component } from "react";
import url from "../../config/url";
import format from "../../helper/format";
export class CheckoutItem extends Component {
  render() {
    return (
      <div className="border-solid border-gray-300 border  rounded p-3 hover:bg-gray-100 mb-5">
        <div className="flex">
          <img
            src={`${url}${this.props.item.image_url}`}
            style={{ width: 90, height: 90 }}
            alt=""
          />
          <div className="pl-3  w-full">
            <div className="font-semibold">{this.props.item.nama}</div>
            <div className="flex justify-between w-full">
              <div>
                {format(this.props.item.qty)} x {format(this.props.item.berat)}{" "}
                gram
              </div>
              <div className="text-orange-400 font-semibold">
                Rp {format(this.props.item.harga * this.props.item.qty)}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CheckoutItem;
