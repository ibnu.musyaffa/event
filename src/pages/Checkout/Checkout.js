import React, { Component } from "react";
import FormItem from "../../components/FormItem";
import { withRouter } from "react-router-dom";
import { Form, Input, message, Icon, Skeleton } from "antd";
import Button from "../../components/Button";
import { connect } from "react-redux";
import * as Yup from "yup";
import { Formik } from "formik";
import axios from "axios";
import url from "../../config/url";
import NavigationLight from "../../components/NavigationLight";
import { Helmet } from "react-helmet";
import format from "../../helper/format";
import createHeaders from "../../helper/headers";
import isAuthenticated from "../../helper/isAuthenticated";
import { setEvent } from "../../store/event";
import { truncateTickets } from "../../store/tickets";
import EventSmall from "../../components/EventSmall";

const CheckoutShape = Yup.object().shape({
  nama: Yup.string().required("Nama wajib diisi"),
  handphone: Yup.string().required("Nomor Handphone wajib diisi"),
  email: Yup.string().required("Email wajib diisi"),
});

export class Checkout extends Component {
  constructor(props) {
    super(props);

    this.state = {
      initialValues: {
        nama: "",
        handphone: "",
        email: "",
      },
      searching: false,
      district: [],
      ongkir: 0,
      ongkirList: [],
      bankList: [],
      location: null,
      status: "s",
      is_authenticated: false,
      isDaftar: false,
      allDisabled: false,
    };

    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    let url = `/${this.props.match.params.username}`;
    if (this.props.user?.name) {
      this.setState({
        initialValues: {
          nama: this.props.user.name,
          handphone: this.props.user.phone_number,
          email: this.props.user.email,
        },
        allDisabled: true,
      });
    }

    if (!this.props.event || this.props.tickets.length == 0) {
      this.props.history.push(url);
    }
  }

  async onSubmit(values, actions) {
    try {
      let newValues = {
        ...values,
        event_id: this.props.event.id,
        tickets: this.props.tickets,
        username: this.props.match.params.username,
      };

      let headers = createHeaders();
      let res = await axios.post(
        `${url}api/event/transaction/checkout`,
        newValues,
        {
          headers,
        }
      );
      let id = res.data.data.id;
      let token = res.data.data.token;
      let checkout_detail_url = `/${this.props.match.params.username}/checkout-detail/${id}/${token}`;
      this.props.truncateTickets();

      this.props.history.push(checkout_detail_url);
      message.info("Detail Invoice telah dikirim ke email anda");
      this.props.setEvent({});
    } catch (error) {
      if (error.response.data.message) {
        // message.error("Terjadi Kesalahan");
        message.error(error.response.data.message);
      } else {
        message.error("Terjadi Kesalahan");
      }
    }
  }

  render() {
    if (!this.props.event?.id) {
      return (
        <div className="w-full">
          <div className="flex justify-center items-center h-full flex-col mb-8">
            <div className="flex flex-col px-5 w-full md:w-1/3 lg:w-1/3 xl:w-1/3 pt-5">
              <Skeleton active></Skeleton>
              <Skeleton active></Skeleton>
              <Skeleton active></Skeleton>
            </div>
          </div>
        </div>
      );
    }
    const total_harga_tiket = this.props.tickets
      .map((item) => item.harga * item.qty)
      .reduce((prev, curr) => prev + curr, 0);

    const jumlah_tiket = this.props.tickets
      .map((item) => item.qty)
      .reduce((prev, curr) => prev + curr, 0);
    return (
      <div>
        <Helmet>
          <title>Checkout - {this.props.match.params.username}</title>
        </Helmet>
        <NavigationLight
          onClick={() => this.props.history.goBack()}
          title="Checkout"
        ></NavigationLight>

        <Formik
          initialValues={this.state.initialValues}
          enableReinitialize={true}
          validationSchema={CheckoutShape}
          onSubmit={this.onSubmit}
        >
          {({
            isSubmitting,
            handleSubmit,
            handleBlur,
            handleChange,
            errors,
            touched,
            values,
            setFieldValue,
            setFieldTouched,
            isValid,
          }) => (
            <Form onSubmit={handleSubmit}>
              <div className="m-5 flex justify-center relative">
                <div className="w-full sm:w-2/3 md:w-2/5 lg:w-1/3">
                  <div className="">
                    <div className="text-gray-700 uppercase font-semibold   text-sm">
                      Informasi personal
                    </div>

                    <FormItem
                      marginBottom="0"
                      label="Nama"
                      error={errors.nama}
                      touched={touched.nama}
                    >
                      <Input
                        onChange={handleChange}
                        onBlur={handleBlur}
                        prefix={
                          <Icon
                            type="user"
                            style={{ color: "rgba(0,0,0,.25)" }}
                          />
                        }
                        value={values.nama}
                        name="nama"
                        size="large"
                        placeholder="Nama Lengkap"
                        disabled={this.state.allDisabled}
                      />
                    </FormItem>
                    <FormItem
                      marginBottom="0"
                      label="Handphone"
                      error={errors.handphone}
                      touched={touched.handphone}
                    >
                      <Input
                        prefix={
                          <Icon
                            type="phone"
                            style={{ color: "rgba(0,0,0,.25)" }}
                          />
                        }
                        onChange={(event) => {
                          setFieldValue(
                            "handphone",
                            event.target.value.replace(/\D/, "")
                          );
                        }}
                        onBlur={handleBlur}
                        value={values.handphone}
                        name="handphone"
                        size="large"
                        placeholder="No. Handphone"
                        disabled={this.state.allDisabled}
                      />
                    </FormItem>
                    <FormItem
                      marginBottom="0"
                      label="Email"
                      error={errors.email}
                      touched={touched.email}
                    >
                      <Input
                        prefix={
                          <Icon
                            type="mail"
                            style={{ color: "rgba(0,0,0,.25)" }}
                          />
                        }
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.email}
                        name="email"
                        size="large"
                        placeholder="Email Aktif"
                        disabled={this.state.allDisabled}
                      />
                    </FormItem>
                  </div>
                  <div className="mt-5">
                    <div className="text-gray-700 font-semibold mb-3 text-sm">
                      Event
                    </div>
                    <EventSmall event={this.props.event}></EventSmall>
                    <div className="text-gray-700 font-semibold mb-3 text-sm">
                      Daftar Tiket
                    </div>
                    <div>
                      {this.props.tickets.map((item, index) => (
                        <div
                          key={index}
                          className="border-solid border-gray-400 mb-4 border p-4 rounded-sm"
                        >
                          <div className="flex flex-col">
                            <div className="flex justify-between items-center mb-2">
                              <div className="font-semibold text-base">
                                {item.nama_tiket}
                              </div>
                              <div className="font-semibold">
                                Rp {format(item.harga)} x {item.qty}
                              </div>
                            </div>
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                  <div className="pt-5">
                    <div className="text-gray-700 font-semibold  mb-3 text-sm">
                      Pembayaran
                    </div>
                    <div className="flex justify-between border-solid border-gray-300 border-b h-12 items-center border-t-0 border-l-0 border-r-0 ">
                      <div>Jumlah Tiket</div>
                      <div>{jumlah_tiket}</div>
                    </div>

                    <div className="flex justify-between h-12 items-center">
                      <div>Total Harga Tiket</div>
                      <div>Rp {format(parseInt(total_harga_tiket))}</div>
                    </div>
                    <div className="my-4 text-center  py-2 px-2 bg-yellow-200">
                      Belum termasuk kode unik pembayaran
                    </div>

                    <Button
                      loading={isSubmitting}
                      type="primary"
                      htmlType="submit"
                      block
                    >
                      Bayar Sekarang
                    </Button>
                  </div>
                </div>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  event: state.event,
  tickets: state.tickets,
  user: state.user,
});

const mapDispatchToProps = (dispatch) => ({
  setEvent: (event) => dispatch(setEvent(event)),
  truncateTickets: () => dispatch(truncateTickets()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Checkout));
