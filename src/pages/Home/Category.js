import React, { Component, useEffect } from "react";
import { Spin, Icon, Modal, Button } from "antd";
import url from "../../config/url";
function ItemColored(props) {
  let active = "";
  if (props.active) {
    active = `border border border-solid border-${props.color}-600`;
  }
  return (
    <div
    style={{ height: 40, width: 'auto' }}
      onClick={props.onClick}
      className={`bg-${props.color}-200 text-${props.color}-600  mr-3 p-1 text-sm flex justify-center items-center rounded cursor-pointer flex ${active} font-semibold whitespace-no-wrap`}
      // className="border border border-solid border-red-400 mr-3 rounded flex justify-center items-center"
    >
      {props.title}
    </div>
  );
}

export class Category extends Component {
  constructor(props) {
    super(props);

    this.state = {
      categoryLeftScroll: 0,
    };
    this.categoryLeftScroll = 0;
    this.categoryReff = React.createRef();
  }

  render() {
    let minScroll = 0;
    let maxScroll = this.categoryReff?.current?.offsetWidth;
    console.log(this.categoryReff.current);
    return (
      <div className="flex mx-5 md:px-1 md:mx-0  mb-8 justify-center items-center">
        <div
          className="cursor-pointer hover:text-teal-600"
          onClick={() => {
            this.categoryLeftScroll =
              this.categoryLeftScroll <= 0
                ? this.categoryLeftScroll
                : this.categoryLeftScroll - 100;

            this.categoryReff.current.scroll({
              top: 0,
              left: this.categoryLeftScroll,
              behavior: "smooth",
            });
          }}
        >
          <Icon type="left" />
        </div>

        <div
          className="flex overflow-x-scroll md:overflow-x-hidden mx-3"
          ref={this.categoryReff}
          style={{ width: "90%" }}
        >
          <ItemColored
            onClick={() => this.props.onChange(null)}
            title={"Semua"}
            active={this.props.activeId == null}
            key={"all"}
            color={"gray"}
            index={99}
          ></ItemColored>
          {this.props.category.map((item, index) => {
            console.log(item);
            return (
              <ItemColored
                onClick={() => this.props.onChange(item.id)}
                title={item.nama}
                active={this.props.activeId == item.id}
                key={index}
                color={item.color ? item.color : "gray"}
                index={index}
              ></ItemColored>
            );
          })}
        </div>

        <div
          className="cursor-pointer hover:text-teal-600"
          onClick={() => {
            let maxScroll = this.categoryReff.current.offsetWidth;
            this.categoryLeftScroll =
              this.categoryLeftScroll > maxScroll
                ? this.categoryLeftScroll
                : this.categoryLeftScroll + 100;
            
            this.categoryReff.current.scroll({
              top: 0,
              left: this.categoryLeftScroll,
              behavior: "smooth",
            });
          }}
        >
          <Icon type="right" />
        </div>
      </div>
    );
  }
}

export default Category;
