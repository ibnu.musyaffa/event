import React, { Component } from "react";
import Headroom from "react-headroom";
import { withRouter } from "react-router-dom";
import { Icon, Badge, Input } from "antd";
import { connect } from "react-redux";

const { Search } = Input;

export class Navigation extends Component {
  constructor(props) {
    super(props);

 
    this.toLogin = this.toLogin.bind(this);
    this.toMenu = this.toMenu.bind(this);

  }


  toLogin() {
    let url = `/${this.props.match.params.username}/login`;
    this.props.history.push(url);
  }

  toMenu() {
    let url = `/${this.props.match.params.username}/menu`;
    this.props.history.push(url);
  }

  render() {
    return (
      <Headroom>
        <div className="w-full flex justify-center bg-white border border-solid border-gray-300 border-t-0 border-r-0 border-l-0  h-16">
          <div className="flex items-center   w-full  md:w-1/3">
            <div className="w-full px-3 md:px-0">
              <Search
                placeholder="Cari Event"
                allowClear
                // onSearch={(value) => console.log(value)}
                onChange={this.props.onChangeSearch}
              />
            </div>
         

            <div
              className="ml-5 pt-1 mr-4 md:mr-0 cursor-pointer"
              onClick={this.toMenu}
            >
              <Icon type="user" style={{ fontSize: 23 }} />
            </div>
          </div>
        </div>
      </Headroom>
    );
  }
}



export default connect(
  null,
  null
)(withRouter(Navigation));
