import React from "react";
import url from "../../config/url";
function HomeFooter(props) {
  let firstTwo = props.setting?.wa?.substring(0, 2);
  let wanumber = null;
  if (firstTwo == "08") {
    wanumber = `62${props.setting.wa.substr(1)}`;
  }
  let waMessage = props.setting?.wa_prefix
  ? encodeURI(props.setting.wa_prefix)
  : "";

  return (
    <div className="flex justify-center items-center mt-5  flex-col">
      <div className="flex flex-col  flex-wrap w-full md:w-1/3 lg:w-1/3 xl:w-1/3 p-5 md:p-0">
        <div className="border-t mt-3 py-3 border-gray-300 border-solid border-l-0 border-r-0 border-b-0"></div>
        <div className="flex flex-wrap">
          <div
            style={{
              flex: "0 0 auto",
              width: "auto",
              maxWidth: "100%",
              marginBottom: 10,
              marginRight: 20,
            }}
          >
            {props.setting?.facebook ||
            props.setting?.instagram ||
            props.setting?.wa ||
            props.setting?.email ? (
              <div className="h-16">
                <div className="font-semibold text-gray-600 text-xs">
                  KONTAK
                </div>
                <div className="flex mt-3">
                  {props.setting?.facebook && (
                    <a
                      target="_blank"
                      rel="noopener noreferrer"
                      href={props.setting?.facebook}
                    >
                      <img
                        style={{ height: 25, marginRight: 8 }}
                        src={require("../../assets/facebook.svg")}
                      ></img>
                    </a>
                  )}
                  {props.setting?.instagram && (
                    <a
                      target="_blank"
                      rel="noopener noreferrer"
                      href={props.setting?.instagram}
                    >
                      <img
                        style={{ height: 25, marginRight: 8 }}
                        src={require("../../assets/instagram.svg")}
                      ></img>
                    </a>
                  )}
                  {wanumber && (
                    <a
                      target="_blank"
                      rel="noopener noreferrer"
                      href={`https://wa.me/${wanumber}?text=${waMessage}`}
                    >
                      <img
                        style={{ height: 25, marginRight: 8 }}
                        src={require("../../assets/whatsapp.svg")}
                      ></img>
                    </a>
                  )}
                  {props.setting?.email && (
                    <a href={`mailto:${props.setting?.email}`}>
                      <img
                        style={{ height: 25, marginRight: 8 }}
                        src={require("../../assets/email.svg")}
                      ></img>
                    </a>
                  )}
                </div>
              </div>
            ) : null}
          </div>
          <div style={{ flexBasis: 0, flexGrow: 1, maxWidth: "100%" }}>
            <div className="h-16">
              <div className="font-semibold text-gray-600 text-xs">
                PEMBAYARAN
              </div>
              <div className="flex mt-2">
                {props.banks.map((item) => (
                  <img
                    key={item.id}
                    className="mr-2"
                    height="32"
                    src={`${url}${item.type.img}`}
                  ></img>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default HomeFooter;
