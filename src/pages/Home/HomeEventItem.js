import React from "react";

export default function HomeEventItem(props) {
  return (
    <div className="max-w-full" onClick={props.onClick}>
      <div className="w-auto border border-gray-300 border-solid rounded hover:bg-gray-100 cursor-pointer">
        <div className="relative">
          <img
            src={props.url}
            alt=""
            className="cursor-pointer hover:opacity-75 w-full rounded-t"
            onClick={props.onClick}
          />
          {props.data.sale_status == "end" && (
            <div
              style={{ opacity: "60%" }}
              className="absolute top-0 font-semibold bg-black w-full h-full z-20  flex justify-center items-center"
            >
              <div className="text-white font-semibold">
                Event telah berakhir
              </div>
            </div>
          )}
        </div>
        <div className="px-2 pt-2 pb-4">
          <div className="font-semibold">{props.data.nama_event}</div>
          <div className="text-orange-500 text-xs">{props.data.cat?.nama? props.data.cat?.nama :'-' }</div>
        </div>
      </div>
    </div>
  );
}
