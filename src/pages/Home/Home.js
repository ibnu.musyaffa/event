import React, { Component } from "react";
import Image from "../../components/Image";
import Navigation from "./Navigation";
import axios from "axios";
import url from "../../config/url";
import { withRouter } from "react-router-dom";
import { Spin, Icon, Modal, Button } from "antd";
import HomeFooter from "./HomeFooter";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { setUser } from "../../store/user";
import HomeProfile from "./HomeProfile";
import debounce from "lodash/debounce";
import HomeEventItem from "./HomeEventItem";
import Category from "./Category";
import Slider from "react-slick";

export class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      events: [],
      status: "fetching",
      setting: null,
      is_valid: false,
      banks: [],
      visibleModal: false,
      search: null,
      category: [],
      category_id: null,
      banner: [],
    };
    this.fetchEvents = this.fetchEvents.bind(this);
    this.onChangeSearch = this.onChangeSearch.bind(this);
    this.searchDebounce = debounce(this.fetchEvents, 500);
    this.onChangeCategory = this.onChangeCategory.bind(this);
  }
  componentDidMount() {
    this.fetchEvents();
  }

  async fetchEvents() {
    try {
      this.setState({
        events: [],
        searchStatus: "fetching",
      });
      let events = await axios.get(`${url}api/event`, {
        params: {
          search: this.state.search,
          username: this.props.match.params.username,
          category_id: this.state.category_id,
        },
      });
      console.log("banner", events.data);
      this.setState({
        events: events.data.data,
        setting: events.data.setting,
        is_valid: Boolean(events.data.setting?.is_valid),
        banks: events.data.banks,
        status: "fetched",
        searchStatus: "fetched",
        category: events.data.category,
        banner: events.data.banner,
      });
    } catch (error) {
      if (error.response?.status == 404) {
        this.props.history.push("/");
      }
    }
  }

  onChangeSearch(event) {
    this.setState(
      {
        search: event.target.value,
      },
      () => {
        this.searchDebounce();
      }
    );
  }

  async onChangeCategory(category_id) {
    this.setState(
      {
        category_id,
      },
      () => {
        this.fetchEvents();
      }
    );
  }

  render() {
    return (
      <div className="w-full">
        <Helmet>
          <title>{this.props.match.params.username}</title>
        </Helmet>
        {this.state.status == "fetching" ? (
          <div className="flex justify-center items-center w-full h-64">
            <Spin size="large"></Spin>
          </div>
        ) : (
          <React.Fragment>
            {this.state.is_valid && (
              <React.Fragment>
                <Navigation
                  user={this.props.user}
                  onChangeSearch={this.onChangeSearch}
                ></Navigation>
                <HomeProfile setting={this.state.setting}></HomeProfile>

                <div className="flex justify-center items-center">
                  <div className="max-w-full overflow-x-hidden md:w-1/3">
                    {this.state.banner.length > 0 && (
                      <div className="mb-5">
                        <Slider
                          className="center"
                          centerMode={true}
                          autoplay
                          pauseOnHover
                          adaptiveHeight
                          infinite={true}
                          slidesToShow
                          speed={500}
                        >
                          {this.state.banner.map((item, index) => {
                            return (
                              <div key={index} className="bg-white">
                                <img
                                  style={{
                                    borderRadius: 5,
                                    marginRight: 8,
                                    marginLeft: 8,
                                    backgroundColor: "white",
                                  }}
                                  src={`${url}${item.image_url}`}
                                />
                              </div>
                            );
                          })}
                        </Slider>
                      </div>
                    )}
                    <div className="">
                      {this.state.category.length > 0 && (
                        <Category
                          activeId={this.state.category_id}
                          onChange={this.onChangeCategory}
                          category={this.state.category}
                        ></Category>
                      )}
                    </div>
                    <div className="product-container  mx-5 md:mx-0">
                      {this.state.events.map((item) => {
                        return (
                          <HomeEventItem
                            key={item.id}
                            url={`${url}${item.image_cover_url}`}
                            onClick={() => {
                              if (item.sale_status != "end") {
                                this.props.history.push(
                                  `/${this.props.match.params.username}/detail/${item.id}`
                                );
                              }
                            }}
                            data={item}
                          ></HomeEventItem>
                        );
                      })}
                    </div>

                    {this.state.searchStatus == "fetching" && (
                      <div className="flex justify-center items-center w-full h-64">
                        <Spin size="large"></Spin>
                      </div>
                    )}

                    {this.state.events.length == 0 &&
                      this.state.searchStatus != "fetching" && (
                        <div className="w-full flex justify-center items-center h-40">
                          <div className="font-semibold text-lg text-gray-600">
                            Event tidak ada
                          </div>
                        </div>
                      )}
                  </div>
                </div>

                <HomeFooter
                  setting={this.state.setting}
                  banks={this.state.banks}
                ></HomeFooter>
              </React.Fragment>
            )}
            {!this.state.is_valid && (
              <div className="flex justify-center items-center flex-col mb-8">
                <div className="flex flex-col px-5   w-full md:w-1/3 lg:w-1/3 xl:w-1/3 justify-center items-center h-screen">
                  <div className="font-semibold text-2xl">
                    Event Belum Aktif
                  </div>
                  <div className="text-center text-lg">
                    Silahkan isi data anda terlebih dahulu dengan lengkap di
                    menu <b>pengaturan event</b>
                  </div>
                </div>
              </div>
            )}
          </React.Fragment>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
});

const mapDispatchToProps = (dispatch) => ({
  setUser: (user) => dispatch(setUser(user)),
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Home));
