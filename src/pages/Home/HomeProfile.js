import React, { Component } from "react";
import { Spin, Icon, Modal, Button } from "antd";
import url from "../../config/url";
export class HomeProfile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      visibleModal: false
    };
  }

  toggleModal = () => {
    this.setState({
      visibleModal: !this.state.visibleModal
    });
  };

  render() {
    return (
      <div className="flex justify-center items-center flex-col mb-8 mt-5">
        <div className="flex flex-row px-5   w-full md:w-1/3 lg:w-1/3 xl:w-1/3">
          <div>
            <img
              className="rounded-full mr-3 shadow"
              src={`${url}/${this.props.setting?.logo_url}`}
              style={{ width: 100, height: 100 }}
              alt=""
            />
          </div>

          <Modal
            title="Detail Alamat"
            visible={this.state.visibleModal}
            onOk={this.toggleModal}
            onCancel={this.toggleModal}
          >
            {this.props.setting?.alamat}
          </Modal>

          <div className="w-full relative flex flex-col px-3">
            <div>
              <div className="font-semibold text-xl">
                {this.props.setting?.nama_toko}
              </div>

              <div className="mt-1">{this.props.setting?.bio}</div>
              <div
                onClick={
                  this.props.setting?.alamat.length > 65
                    ? this.toggleModal
                    : null
                }
                className="mt-1 text-teal-700 cursor-pointer hover:text-teal-500"
              >
                <Icon
                  type="environment"
                  style={{ marginRight: 7, color: "#1c9fad" }}
                />
                {this.props.setting?.alamat.length > 65 ? (
                  <React.Fragment>
                    {this.props.setting?.alamat.substring(0, 65)}
                    ......
                  </React.Fragment>
                ) : (
                  this.props.setting?.alamat
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default HomeProfile;
