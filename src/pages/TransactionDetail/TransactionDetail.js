import React, { Component } from "react";
import { message, Spin, Icon } from "antd";
import { withRouter } from "react-router-dom";
import format from "../../helper/format";
import axios from "axios";
import url from "../../config/url";
import Item from "../../components/ProductItem";
import EventSmall from "../../components/EventSmall";
import NavigationLight from "../../components/NavigationLight";
import { Helmet } from "react-helmet";
import CustomerInfo from "../../components/CustomerInfo";
import Button from "../../components/Button";

function Row(props) {
  let gray = props.bgGray ? "bg-gray-100" : "";
  return (
    <div className={`flex justify-between  py-2  ${gray}`}>
      <div className="text-gray-700 font-medium">{props.label}</div>
      <div className="text-gray-900">{props.children}</div>
    </div>
  );
}
export class CheckoutDetail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      bankList: [],
      showPembayaran: true,
      status: "fetching",
      transaction: {
        id: null,
        user_id: null,
        nama: null,
        handphone: null,
        email: null,
        total: 217000,
        expired_date: null,
        status: null,
        created_at: null,
        kode_unik: null,
        ongkir: null,
      },
    };
    this.toConfimation = this.toConfimation.bind(this);
    this.toTickets = this.toTickets.bind(this);
  }

  componentDidMount() {
    this.fetchTransaction();
  }

  async fetchTransaction() {
    try {
      let username = this.props.match.params.username;
      let id = this.props.match.params.id;
      let token = this.props.match.params.token;
      let res = await axios.get(`${url}api/event/transaction/${id}/${token}`);
      let status = res.data.data.status;
      if (status == "UNPAID" || status == "EXPIRED") {
        this.props.history.push(`/${username}/checkout-detail/${id}/${token}`);
        return;
      }
      this.setState({
        status: "fetched",
        transaction: res.data.data,
      });
    } catch (error) {}
  }

  copied() {
    message.success("Berhasil dicopy");
  }

  toConfimation() {
    let id = this.props.match.params.id;
    let token = this.props.match.params.token;
    let username = this.props.match.params.username;

    let url = `/${username}/konfirmasi-pembayaran/${id}/${token}`;
    this.props.history.push(url);
  }

  toTickets() {
    let id = this.props.match.params.id;
    let token = this.props.match.params.token;
    let username = this.props.match.params.username;

    let url = `/${username}/tickets/${id}/${token}`;
    this.props.history.push(url);
  }
  render() {
    if (this.state.status == "fetching") {
      return (
        <div>
          <div className="flex justify-center items-center h-64">
            <Spin size="large" />
          </div>
        </div>
      );
    }

    return (
      <div>
        <Helmet>
          <title>Detail Transaksi - {this.props.match.params.username}</title>
        </Helmet>
        <NavigationLight
          onClick={() => this.props.history.goBack()}
          title="Detail Transaksi"
        ></NavigationLight>
        <div className="m-5 flex justify-center">
          <div className="w-full sm:w-2/3 md:w-2/5 lg:w-1/3">
            <div className="font-bold mb-2 text-lg">Data Pemesan</div>
            <CustomerInfo transaction={this.state.transaction}></CustomerInfo>
            <div className="mt-5">
              <div className="font-bold mb-2 text-lg">Event</div>
              <EventSmall event={this.state.transaction.event}></EventSmall>
            </div>

            <div className="flex justify-between">
              <div className="font-bold mb-2 text-lg">Pembayaran</div>
              <div
                className="flex cursor-pointer hover:text-teal-500"
                onClick={() =>
                  this.setState({ showPembayaran: !this.state.showPembayaran })
                }
              ></div>
            </div>
            {this.state.showPembayaran && (
              <div className="">
                <Row label="Total harga tiket">
                  Rp{" "}
                  {format(
                    this.state.transaction.total -
                      this.state.transaction.kode_unik
                  )}
                </Row>
                <Row label="Kode unik">
                  Rp {format(this.state.transaction.kode_unik)}
                </Row>
                <Row bgGray label="Total Pembayaran">
                  Rp {format(this.state.transaction.total)}
                </Row>
                {this.state.transaction.total != 0 && (
                  <React.Fragment>
                    <Row label="Bukti Transfer">
                      <a
                        target="_blank"
                        href={`${url}${this.state.transaction.bukti_transfer_url}`}
                      >
                        Klik untuk melihat
                      </a>
                    </Row>
                    <Row bgGray label="Tanggal Transfer">
                      {this.state.transaction.tanggal_transfer}
                    </Row>
                  </React.Fragment>
                )}

                <Row label="Status Pembayaran">
                  {this.state.transaction.status_pembayaran == "CONFIRMED" && (
                    <div className="bg-yellow-300 text-yellow-800 inline p-1 rounded font-semibold text-xs">
                      {this.state.transaction.status_pembayaran}
                    </div>
                  )}
                  {this.state.transaction.status_pembayaran == "PAID" && (
                    <div className="bg-green-300 text-green-800 inline p-1 rounded font-semibold text-xs">
                      {this.state.transaction.status_pembayaran}
                    </div>
                  )}
                </Row>

                {this.state.transaction.total != 0 && (
                  <div className="flex justify-between flex-col  py-2 px-1">
                    <div className="text-gray-700 font-medium mt-1 mb-2">
                      Metode Pembayaran
                    </div>
                    <div>
                      <img
                        className="object-scale-down"
                        style={{ height: 40, width: 130 }}
                        src={`${url}${this.state.transaction.bank_image_url}`}
                      ></img>
                    </div>
                    <div className="mt-1">
                      <div className="font-semibold">
                        {this.state.transaction.nama_bank}{" "}
                      </div>
                      <div className="mt-1">
                        {this.state.transaction.nomor_rekening}
                      </div>
                      <div className="mt-1">
                        atas nama{" "}
                        <span className="font-semibold">
                          {this.state.transaction.nama_rekening}
                        </span>
                      </div>
                    </div>
                  </div>
                )}

                {this.state.transaction.status_pembayaran == "CONFIRMED" && (
                  <div className="text-center mt-3 mb-3 bg-yellow-200 rounded p-5">
                    Transaksi dengan status <b>CONFIRMED</b> membutuhkan waktu
                    pengecekan oleh admin, mohon menunggu
                  </div>
                )}
              </div>
            )}

            {this.state.transaction.status_pembayaran == "PAID" && (
              <div className="mt-5">
                <Button
                  size="large"
                  type="primary"
                  block
                  onClick={this.toTickets}
                >
                  Lihat Tiket
                </Button>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(CheckoutDetail);
