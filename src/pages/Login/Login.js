import React, { Component } from "react";
import { Form, Icon, Input, message } from "antd";
import url from "../../config/url";
import Button from "../../components/ButtonTeal";
import FormItem from "../../components/FormItem";
import { withRouter } from "react-router-dom";
import Navigation from "../../components/Navigation";
import { Formik } from "formik";
import * as Yup from "yup";
import Auth from "../../api/auth";
import { setUser } from "../../store/user";

import { connect } from "react-redux";

const LoginShape = Yup.object().shape({
  user: Yup.string().required("Username wajib diisi"),
  password: Yup.string().required("Password wajib diisi")
});

export class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      errorMessage: ""
    };
    this.toRegister = this.toRegister.bind(this);
    this.toForgorPassword = this.toForgorPassword.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  toRegister() {
    let url = `/${this.props.match.params.username}/register`;
    this.props.history.push(url);
  }

  toForgorPassword() {
    let url = `/${this.props.match.params.username}/forgot-password`;
    this.props.history.push(url);
  }

  async onSubmit(values, actions) {
    try {
      let response = await Auth.login(values);
      if (response.data.code == "00") {
        localStorage.setItem("token", response.data.data.access_token);
        let url = `/${this.props.match.params.username}`;
        this.props.setUser(response.data.data.user);
        this.props.history.push(url);
        message.success("Login berhasil");
      } else {
        if (response.data.code == -1) {
          message.error(response.data.info);
          let url = `/${this.props.match.params.username}/register-verifikasi`;
          this.props.history.push(url, { email: response.data.data.email });
        }
        this.setState({
          errorMessage: response.data.info
        });
      }
    } catch (err) {
      this.setState({
        errorMessage: "Something went wrong"
      });
    }
  }

  render() {
    return (
      <div className="w-full">
        <Navigation withBack noCart title="Login"></Navigation>
        <div className="flex justify-center items-center h-full flex-col mb-8">
          <div className="flex flex-col px-5 w-full md:w-1/3 lg:w-1/3 xl:w-1/3">
            <div className="flex justify-center items-center h-32">
              <img height="30" src={`${url}/images/footer.png`}></img>
            </div>
            <div className="mx-3 md:mx-0">
              {this.state.errorMessage && (
                <div className="text-center p-2 bg-red-200 text-red-800 rounded-sm mb-4">
                  {this.state.errorMessage}
                </div>
              )}

              <Formik
                initialValues={{ user: "", password: "" }}
                validationSchema={LoginShape}
                onSubmit={this.onSubmit}
              >
                {({
                  isSubmitting,
                  handleSubmit,
                  handleBlur,
                  handleChange,
                  errors,
                  touched,
                  values,
                  setFieldValue,
                  setFieldTouched
                }) => (
                  <Form onSubmit={handleSubmit}>
                    <FormItem error={errors.user} touched={touched.user}>
                      <Input
                        prefix={
                          <Icon
                            type="user"
                            style={{ color: "rgba(0,0,0,.25)" }}
                          />
                        }
                        name="user"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.user}
                        placeholder="Username"
                      />
                    </FormItem>
                    <FormItem
                      error={errors.password}
                      touched={touched.password}
                    >
                      <Input
                        prefix={
                          <Icon
                            type="lock"
                            style={{ color: "rgba(0,0,0,.25)" }}
                          />
                        }
                        type="password"
                        name="password"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.password}
                        placeholder="Password"
                      />
                    </FormItem>
                    <FormItem>
                      <a
                        className="login-form-forgot float-right"
                        onClick={this.toForgorPassword}
                      >
                        Lupa Password
                      </a>
                      <Button
                        loading={isSubmitting}
                        type="primary"
                        htmlType="submit"
                        block
                      >
                        Log in
                      </Button>
                      <div className="flex justify-center">
                        <span>Belum punya akun ? </span>

                        <span
                          className="text-blue-600 cursor-pointer text-center ml-1"
                          onClick={this.toRegister}
                        >
                          {" "}
                          Daftar sekarang
                        </span>
                      </div>
                    </FormItem>
                  </Form>
                )}
              </Formik>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user
});

const mapDispatchToProps = dispatch => ({
  setUser: user => dispatch(setUser(user))
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Login));
