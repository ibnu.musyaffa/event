import React, { Component } from "react";
import { message, Spin } from "antd";
import { withRouter } from "react-router-dom";
import format from "../../helper/format";
import axios from "axios";
import url from "../../config/url";
import Item from "../../components/ProductItem";
import NavigationLight from "../../components/NavigationLight";
import { Helmet } from "react-helmet";
import CustomerInfo from "../../components/CustomerInfo";

function Catatan(props) {
  return (
    <React.Fragment>
      <div className="px-1 font-bold  mt-5 text-base">Catatan</div>
      <div className="px-1 mt-1 mb-5">{props.transaction.catatan}</div>
    </React.Fragment>
  );
}

function Row(props) {
  let gray = props.bgGray ? "bg-gray-100" : "";
  return (
    <div className={`flex justify-between  py-2 px-1 ${gray}`}>
      <div className="text-gray-700 font-medium">{props.label}</div>
      <div className="text-gray-900">{props.children}</div>
    </div>
  );
}
export class CheckoutDetail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      bankList: [],
      isFetching: true,
      transaction: {
        id: null,
        user_id: null,
        nama: null,
        handphone: null,
        email: null,
        total: 217000,
        expired_date: null,
        status: null,
        created_at: null,
        kode_unik: null,
        ongkir: null
      }
    };
    this.toConfimation = this.toConfimation.bind(this);
  }

  componentDidMount() {
    this.fetchTransaction();
  }

  async fetchTransaction() {
    try {
      let username = this.props.match.params.username;
      let id = this.props.match.params.id;
      let res = await axios.get(`${url}api/transaction/${id}`);
      console.log(res.data.data);
      if (
        res.data.data.status == "UNPAID" ||
        res.data.data.status == "EXPIRED"
      ) {
        this.props.history.push(`/${username}/checkout-detail/${id}`);
      }
      this.setState({
        transaction: res.data.data
      });
    } catch (error) {}
  }

  copied() {
    message.success("Berhasil dicopy");
  }

  toConfimation() {
    let id = this.props.match.params.id;
    let username = this.props.match.params.username;

    let url = `/${username}/konfirmasi-pembayaran/${id}`;
    this.props.history.push(url);
  }
  render() {
    let expedisi = this.state.transaction?.expedisi?.split("+");
    if (
      this.state.transaction.status == "CONFIRMED" ||
      this.state.transaction.status == "PAID"
    ) {
      return (
        <div>
          <Helmet>
            <title>Status Transaksi - {this.props.match.params.username}</title>
          </Helmet>
          <div
            style={{ height: 200 }}
            className="absolute top-0 w-full z-0 pattern"
          ></div>
          <div className="m-5 flex justify-center">
            <div className="w-full sm:w-2/3 md:w-2/5 lg:w-1/3">
              <NavigationLight
                onClick={() => this.props.history.goBack()}
                title="Status Transaksi"
              ></NavigationLight>
              <CustomerInfo transaction={this.state.transaction}></CustomerInfo>
              <div className="">
                <div className="px-1 mt-5 font-bold mb-3 text-base">
                  Daftar Produk / Jasa
                </div>
                {this.state.transaction?.products?.map(item => (
                  <Item item={item}></Item>
                ))}
                <Catatan transaction={this.state.transaction}></Catatan>
                <div className="px-1 font-bold mb-2 text-base">Pembayaran</div>
                <div className="">
                  <Row label="Total harga barang/jasa">
                    Rp {format(this.state.transaction.total_harga_barang)}
                  </Row>
                  <Row bgGray label="Total Ongkos kirim">
                    Rp {format(this.state.transaction.ongkir)}
                  </Row>
                  <Row label="Kode unik">
                    Rp {format(this.state.transaction.kode_unik)}
                  </Row>
                  <Row bgGray label="Total Pembayaran">
                    Rp {format(this.state.transaction.total)}
                  </Row>
                  <Row label="Bukti Transfer">
                    <a
                      target="_blank"
                      href={`${url}${this.state.transaction.bukti_transfer}`}
                    >
                      Klik untuk melihat
                    </a>
                  </Row>
                  <Row bgGray label="Tanggal Transfer">
                    {this.state.transaction.transfer_date}
                  </Row>
                  <Row label="Status Pembayaran">
                    {this.state.transaction.status == "CONFIRMED" && (
                      <div className="bg-yellow-300 text-yellow-800 inline p-1 rounded font-semibold text-xs">
                        {this.state.transaction.status}
                      </div>
                    )}
                    {this.state.transaction.status == "PAID" && (
                      <div className="bg-green-300 text-green-800 inline p-1 rounded font-semibold text-xs">
                        {this.state.transaction.status}
                      </div>
                    )}
                  </Row>

                  <div className="flex justify-between flex-col  py-2 px-1">
                    <div className="text-gray-700 font-medium mt-1 mb-2">
                      Metode Pembayaran
                    </div>
                    <div>
                      <img
                        className="object-scale-down"
                        style={{ height: 40, width: 130 }}
                        src={`${url}${this.state.transaction.bank_image}`}
                      ></img>
                    </div>
                    <div className="mt-1">
                      <div className="font-semibold">
                        {this.state.transaction.nama_bank}{" "}
                      </div>
                      <div className="mt-1">
                        {this.state.transaction.nomor_rekening}
                      </div>
                      <div className="mt-1">
                        atas nama{" "}
                        <span className="font-semibold">
                          {this.state.transaction.nama_rekening}
                        </span>
                      </div>
                    </div>
                  </div>

                  {this.state.transaction.status == "CONFIRMED" && (
                    <div className="text-center mt-3 mb-3 bg-yellow-200 rounded p-5">
                      Transaksi dengan status <b>CONFIRMED</b> membutuhkan waktu
                      pengecekan oleh admin, mohon menunggu
                    </div>
                  )}
                </div>

                {this.state.transaction?.expedisi && (
                  <React.Fragment>
                    <div className="px-1 font-bold mb-2 mt-5 text-base">
                      Pengiriman
                    </div>
                    <div className="my-3 px-1">
                      <div>
                        <div className="text-gray-700 font-medium">
                          Expedisi
                        </div>
                        <div className="text-gray-900 uppercase">{`${expedisi[0]} ${expedisi[1]}`}</div>
                      </div>
                      <div className="mt-3">
                        <div className="text-gray-700 font-medium">
                          Alamat Pengiriman
                        </div>
                        <div className="text-gray-900">
                          {this.state.transaction.alamat}
                        </div>
                      </div>

                      <div className="mt-3">
                        <div className="text-gray-700 font-medium">
                          Status Pengiriman
                        </div>
                        <div className="py-2">
                          {this.state.transaction.status_pengiriman ==
                            "DIKIRIM" && (
                            <div className="bg-green-300 text-green-800 inline p-1 rounded font-semibold text-xs">
                              {this.state.transaction.status_pengiriman}
                            </div>
                          )}
                          {this.state.transaction.status_pengiriman ==
                            "BELUM DIKIRIM" && (
                            <div className="bg-yellow-300 text-yellow-800 inline p-1 rounded font-semibold text-xs">
                              {this.state.transaction.status_pengiriman}
                            </div>
                          )}
                          {this.state.transaction.status_pengiriman ==
                            "DIPACKING" && (
                            <div className="bg-blue-300 text-blue-800 inline p-1 rounded font-semibold text-xs">
                              {this.state.transaction.status_pengiriman}
                            </div>
                          )}
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="text-gray-700 font-medium">
                          Nomor Resi
                        </div>
                        <div className="text-gray-900">
                          {this.state.transaction.resi
                            ? this.state.transaction.resi
                            : "Belum tersedia"}
                        </div>
                      </div>
                    </div>
                  </React.Fragment>
                )}
              </div>
            </div>
          </div>
        </div>
      );
    }

    return (
      <div>
        <div className="flex justify-center items-center h-64">
          <Spin size="large" />
        </div>
      </div>
    );
  }
}

export default withRouter(CheckoutDetail);
