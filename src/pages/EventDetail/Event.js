import React, { Component, useState } from "react";
import { Button, Input, Icon } from "antd";
import ButtonCustom from "../../components/Button";
import format from "../../helper/format";
import url from "../../config/url";
import {
  FacebookShareButton,
  FacebookIcon,
  TwitterShareButton,
  TwitterIcon,
  WhatsappShareButton,
  WhatsappIcon,
  TelegramShareButton,
  TelegramIcon,
} from "react-share";
import { syncTicket } from "../../store/tickets";

import { connect } from "react-redux";

function TabPane(props) {
  return <div>{props.children}</div>;
}

function Tabs(props) {
  let [selected, setSelected] = useState(0);
  return (
    <React.Fragment>
      <div className="flex justify-between h-12 mt-5">
        {props.children.map((element, index) => {
          let activeClass = "text-teal-800";
          let style = index == selected ? activeClass : "";
          return (
            <div
              key={index}
              className={`w-1/2 font-semibold relative flex hover:bg-teal-100 hover:text-teal-800 justify-center items-center cursor-pointer ${style}`}
              onClick={() => {
                setSelected(index);
              }}
            >
              {element.props.title}
              {index == selected && (
                <div className=" w-full absolute bottom-0 border-teal-500 border-b-3 border-solid border-t-0 border-l-0 border-r-0"></div>
              )}
            </div>
          );
        })}
      </div>
      <div className="mt-5">{props.children[selected]}</div>
    </React.Fragment>
  );
}

function Ticket(props) {
  let ticket = props.ticket;
  let [qty, setQty] = useState(0);

  let onIncrement = () => {
    let currentQty = qty < ticket.qty ? qty + 1 : qty;
    props.syncTicket({
      ...ticket,
      max_qty: ticket.qty,
      qty: currentQty,
    });
    setQty(currentQty);
  };

  let onDecrement = () => {
    let currentQty = qty > 0 ? qty - 1 : qty;
    props.syncTicket({
      ...ticket,
      max_qty: ticket.qty,
      qty: currentQty,
    });
    setQty(currentQty);
  };

  let tag = <div></div>;
  if (ticket.sell_status_reason == "out_stock") {
    tag = (
      <div className="bg-red-200 text-red-900 p-1 px-2 rounded-sm">
        Tiket Habis
      </div>
    );
  }

  if (ticket.sell_status_reason == "on_sale") {
    tag = <div>Penjualan s/d {ticket.tanggal_end}</div>;
  }

  if (ticket.sell_status_reason == "sale_ended") {
    tag = (
      <div className="bg-red-200 text-red-900 p-1 px-2 rounded-sm">
        Penjualan berakhir
      </div>
    );
  }

  if (ticket.sell_status_reason == "not_started") {
    tag = (
      <div className="bg-green-200 text-green-900 p-1 px-2 rounded-sm">
        Penjualan belum dimulai
      </div>
    );
  }

  return (
    <div className="border-solid border-gray-400 mb-5 border p-4">
      <div className="flex flex-col">
        <div className="flex justify-between items-center mb-2">
          <div className="font-semibold text-base">{ticket.nama_tiket}</div>
          <div className="font-semibold">Rp {format(ticket.harga)}</div>
        </div>
        <div className="flex flex-col md:flex-row md:justify-between">
          <div>
            <div className="text-xs">{tag}</div>
            <div className="text-xs mt-1 text-red-600">Sisa Tiket : {ticket.qty}</div>
          </div>

          <div className="w-1/2 mt-3 flex self-end md:w-1/3 md:block">
            <Input
              value={qty}
              addonAfter={
                <Button
                  disabled={!ticket.is_sell || ticket.qty == 0}
                  size="small"
                  onClick={onIncrement}
                  type="link"
                  icon="plus"
                />
              }
              addonBefore={
                <Button
                  disabled={!ticket.is_sell || ticket.qty == 0}
                  onClick={onDecrement}
                  size="small"
                  type="link"
                  icon="minus"
                />
              }
            ></Input>
          </div>
        </div>
      </div>
    </div>
  );
}
export class Event extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {}

  render() {
    let event = this.props.event;
    return (
      <div className="flex justify-center flex-col mb-5">
        <img
          className="w-full cursor-pointer hover:opacity-75"
          src={`${url}${event.image_cover_url}`}
          alt=""
        />

        <div className="p-4 md:p-0 lg:p-0 xl:p-0 md:pt-4 lg:pt-4 xl:pt-4">
          <div>
            <div className="text-gray-800 font-semibold text-lg">
              {event.nama_event}
            </div>
            <div className="text-orange-500 mt-1">
              {event.cat?.nama ? event.cat?.nama : "-"}
            </div>
          </div>
          <div className="border-b border-solid border-r-0 border-l-0 border-t-0 border-gray-400 my-4"></div>

          <div className="mt-5">
            <div className="mb-2 font-semibold text-sm">
              Diselenggarakan oleh
            </div>
            <div className="flex items-center">
              <img
                className="rounded-full h-12 mr-3 object-cover"
                style={{ height: 50, width: 50 }}
                src={`${url}${event.logo_organizer_url}`}
              ></img>
              <div className="text-teal-500 font-semibold">
                {event.nama_organizer}
              </div>
            </div>
          </div>
          <div className="mt-5">
            <div className="mb-2 font-semibold text-sm">Tanggal dan Waktu</div>
            <div className="flex items-center">
              <div className="mr-3 mt-1">
                <Icon
                  className="text-lg"
                  style={{ color: "#1c9fad" }}
                  type="calendar"
                />
              </div>
              <div>
                {event.tanggal_start == event.tanggal_end ? (
                  event.tanggal_start
                ) : (
                  <React.Fragment>
                    {event.tanggal_start} - {event.tanggal_end}
                  </React.Fragment>
                )}
              </div>
            </div>
            <div className="flex items-center">
              <div className="mr-3 mt-1">
                <Icon
                  className="text-lg"
                  style={{ color: "#1c9fad" }}
                  type="clock-circle"
                />
              </div>

              <div>
                {event.waktu_start} - {event.waktu_end} WIB
              </div>
            </div>
          </div>
          <div className="mt-5">
            <div className="mb-2 font-semibold text-sm">Lokasi</div>
            <div className="flex ">
              <div className="mr-3">
                <Icon
                  className="text-lg"
                  style={{ color: "#1c9fad" }}
                  type="home"
                />
              </div>

              <div>{event.nama_tempat}</div>
            </div>
            <div className="flex mt-2">
              <div className="mr-3">
                <Icon
                  className="text-lg"
                  style={{ color: "#1c9fad" }}
                  type="environment"
                />
              </div>

              <div>
                {event.alamat}
                {event.map_link && (
                  <React.Fragment>
                    {"   -   "}
                    <a
                      href={event.map_link}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      Lihat Peta
                    </a>
                  </React.Fragment>
                )}{" "}
              </div>
            </div>

            <div className="mt-5">
              <div className="mb-2 font-semibold text-sm">Bagikan</div>
              <div>
                <FacebookShareButton
                  url={window.location.href}
                  style={{ marginRight: 5 }}
                >
                  <FacebookIcon size={32} round={true} />
                </FacebookShareButton>
                <TwitterShareButton
                  url={window.location.href}
                  style={{ marginRight: 5 }}
                >
                  <TwitterIcon size={32} round={true} />
                </TwitterShareButton>
                <WhatsappShareButton
                  url={window.location.href}
                  style={{ marginRight: 5 }}
                >
                  <WhatsappIcon size={32} round={true} />
                </WhatsappShareButton>
                <TelegramShareButton
                  url={window.location.href}
                  style={{ marginRight: 5 }}
                >
                  <TelegramIcon size={32} round={true} />
                </TelegramShareButton>
              </div>
            </div>
          </div>

          <Tabs>
            <TabPane title="DESKRIPSI EVENT">
              <div
                dangerouslySetInnerHTML={{ __html: event.description }}
              ></div>
            </TabPane>
            <TabPane title="KATEGORI TIKET">
              {event.tickets.map((item, index) => (
                <Ticket
                  syncTicket={this.props.syncTicket}
                  key={index}
                  ticket={item}
                ></Ticket>
              ))}
              {event.tickets.length == 0 && (
                <div
                  className="flex justify-center items-center font-semibold text-lg text-gray-600"
                  style={{ height: 100 }}
                >
                  <div>Tiket belum tersedia</div>
                </div>
              )}
            </TabPane>
          </Tabs>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  syncTicket: (tickets) => dispatch(syncTicket(tickets)),
});

export default connect(null, mapDispatchToProps)(Event);
