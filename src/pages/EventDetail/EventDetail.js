import React, { Component } from "react";

import { withRouter } from "react-router-dom";
import Event from "./Event";
import Navigation from "../../components/Navigation";
import axios from "axios";
import url from "../../config/url";
import { Spin, message } from "antd";
import createHeaders from "../../helper/headers";
import { connect } from "react-redux";
import { setEvent } from "../../store/event";
import { syncTicket, truncateTickets } from "../../store/tickets";
import { Helmet } from "react-helmet";
import format from "../../helper/format";
import ButtonCustom from "../../components/Button";
export class Detail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      event: [],
      status: "fetching",
      statusButton: "idle",
    };
    this.onCheckout = this.onCheckout.bind(this);
  }

  componentDidMount() {
    this.fetchEvent();
    this.props.truncateTickets();
  }

  async onCheckout(values, actions) {
    try {
      this.setState({
        statusButton: "loading",
      });
      let newValues = {
        event_id: this.props.event.id,
        tickets: this.props.tickets,
        username: this.props.match.params.username,
      };

      let headers = createHeaders();
      let res = await axios.post(
        `${url}api/event/transaction/before-checkout`,
        newValues,
        {
          headers,
        }
      );
      let checkout_detail_url = `/${this.props.match.params.username}/checkout`;
      this.props.history.push(checkout_detail_url);
    } catch (error) {
      this.setState({
        statusButton: "idle",
      });
      if (error.response.data.message) {
        message.error(error.response.data.message);
      } else {
        message.error("Terjadi Kesalahan");
      }
    }
  }

  async fetchEvent() {
    try {
      let event = await axios.get(
        `${url}api/event/${this.props.match.params.id}?username=${this.props.match.params.username}`
      );

      this.props.setEvent(event.data.data);
      this.setState({
        event: event.data.data,
        status: "idle",
      });
    } catch (error) {
      this.setState({
        status: "error",
      });
      this.props.setEvent({});
    }
  }

  render() {
    if (this.state.status == "fetching") {
      return (
        <div>
          <div className="flex justify-center items-center h-64">
            <Spin size="large" />
          </div>
        </div>
      );
    }

    if (this.state.status == "error") {
      return (
        <div>
          <div className="flex justify-center items-center h-64 font-semibold text-lg">
            Event tidak ditemukan
          </div>
        </div>
      );
    }

    const total_harga_tiket = this.props.tickets
      .map((item) => item.harga * item.qty)
      .reduce((prev, curr) => prev + curr, 0);
    const total_tiket = this.props.tickets
      .map((item) => item.qty)
      .reduce((prev, curr) => prev + curr, 0);
    return (
      <div className="w-full">
        <Helmet>
          <title>{this.props.match.params.username}</title>
        </Helmet>

        <Navigation title="Detail Event" withBack></Navigation>

        <div className="flex justify-center">
          <div className="w-full md:w-1/3 lg:w-1/3 xl:w-1/3">
            <div>
              <Event event={this.state.event}></Event>
            </div>

            <div className="w-full mb-5 ">
              <div className="flex  mx-5 md:mx-0 justify-between items-center mb-3 border border-gray-300 border-solid bg-gray-100 p-3 ">
                <div className="flex items-center justify-center">
                  <div className="mr-1">Qty : </div>
                  <div className="font-semibold">{total_tiket}</div>
                </div>
                <div className="flex items-center justify-center">
                  <div className="mr-1">Total : </div>
                  <div className="font-semibold">
                    Rp {format(total_harga_tiket)}
                  </div>
                </div>
              </div>
              <div className="mx-5 md:mx-0 mb-4">
                <ButtonCustom
                  onClick={this.onCheckout}
                  loading={this.state.statusButton == "loading"}
                  disabled={total_tiket <= 0}
                  size="large"
                  type="primary"
                  block
                >
                  Beli
                </ButtonCustom>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  tickets: state.tickets,
  event: state.event,
});

const mapDispatchToProps = (dispatch) => ({
  setEvent: (event) => dispatch(setEvent(event)),
  truncateTickets: () => dispatch(truncateTickets()),
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Detail));
