import React, { Component } from "react";
import { Form, Icon, Input, Checkbox, message, Skeleton } from "antd";
import Button from "../../components/ButtonTeal";
import url from "../../config/url";
import axios from "axios";
import FormItem from "../../components/FormItem";
import { withRouter } from "react-router-dom";
import Navigation from "../../components/Navigation";
import { Formik } from "formik";
import * as Yup from "yup";
import createHeaders from "../../helper/headers";

const RegisterShape = Yup.object().shape({
  name: Yup.string().required("Nama wajib diisi"),
  username: Yup.string().required("Username wajib diisi"),
  email: Yup.string()
    .required("Email wajib diisi")
    .email("Email tidak valid"),
  phone_number: Yup.string().required("Nomor handphone wajib diisi")
});

export class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      initialValues: {
        name: "",
        username: "",
        email: "",
        phone_number: ""
      },
      status: "fetching"
    };
    this.onSubmit = this.onSubmit.bind(this);
  }
  componentDidMount() {
    this.fetchProfile();
  }
  async fetchProfile() {
    try {
      this.setState({ status: "fetching" });
      let headers = createHeaders();
      let res = await axios.get(`${url}api/profile/custom`, {
        headers
      });

      let user = res.data.data.user;

      if (res.data.data.user != null) {
        this.setState({
          initialValues: {
            ...user,
            phone_number: user.phone_number ? user.phone_number : ""
          }
        });
      }
      this.setState({ status: "success" });
    } catch (error) {
      this.setState({ status: "error" });
      message.error("Terjadi Kesalahan");
    }
  }

  async onSubmit(values, actions) {
    try {
      let headers = createHeaders();
      let res = await axios.post(`${url}api/profile`, values, {
        headers
      });
      console.log(res.data.code);
      if (res.data.code == "00") {
        message.success(res.data.info);
        let url = `/${this.props.match.params.username}/register-verifikasi`;
        this.props.history.push(url, { email: values.email });
      } else {
        message.error(res.data.info);
      }
    } catch (err) {}
  }
  render() {
    if (this.state.status == "fetching") {
      return (
        <div className="w-full">
          <div className="flex justify-center items-center h-full flex-col mb-8">
            <div className="flex flex-col px-5 w-full md:w-1/3 lg:w-1/3 xl:w-1/3 pt-5">
              <Skeleton active></Skeleton>
              <Skeleton active></Skeleton>
            </div>
          </div>
        </div>
      );
    }
    return (
      <div className="w-full">
        <Navigation withBack noCart title="Edit Profile"></Navigation>
        <div className="flex justify-center items-center h-full flex-col mb-8">
          <div className="flex flex-col px-5 w-full md:w-1/3 lg:w-1/3 xl:w-1/3 pt-5">
            <div className="mx-3 md:mx-0">
              <Formik
                initialValues={this.state.initialValues}
                enableReinitialize
                validationSchema={RegisterShape}
                onSubmit={this.onSubmit}
              >
                {({
                  isSubmitting,
                  handleSubmit,
                  handleBlur,
                  handleChange,
                  errors,
                  touched,
                  values,
                  setFieldValue,
                  setFieldTouched
                }) => (
                  <Form onSubmit={handleSubmit}>
                    <FormItem
                      error={errors.name}
                      touched={touched.name}
                      label="Nama Lengkap"
                      marginBottom="0.1rem"
                    >
                      <Input
                        name="name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.name}
                        placeholder="Nama Lengkap"
                      />
                    </FormItem>
                    <FormItem
                      error={errors.username}
                      touched={touched.username}
                      label="Username"
                      marginBottom="0.1rem"
                    >
                      <Input
                        name="username"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.username}
                        disabled={values.username == this.props.match.params.username}
                        addonBefore="berbagi.link/"
                        type="text"
                        placeholder="Username"
                      />
                    </FormItem>
                    <FormItem
                      error={errors.email}
                      touched={touched.email}
                      label="Email"
                      marginBottom="0.1rem"
                    >
                      <Input
                        name="email"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.email}
                        type="text"
                        disabled
                        placeholder="Email"
                      />
                    </FormItem>
                    <FormItem
                      error={errors.phone_number}
                      touched={touched.phone_number}
                      label="Nomor Handphone"
                      // marginBottom="0.1rem"
                    >
                      <Input
                        name="phone_number"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.phone_number}
                        type="text"
                        placeholder="Nomor Handphone"
                      />
                    </FormItem>
                    <FormItem>
                      <Button
                        type="primary"
                        htmlType="submit"
                        block
                        loading={isSubmitting}
                      >
                        Submit
                      </Button>
                    </FormItem>
                  </Form>
                )}
              </Formik>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Login);
