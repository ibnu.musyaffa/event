import React, { Component } from "react";
import { Form, Icon, Input, Checkbox, message } from "antd";
import Button from "../../components/ButtonTeal";
import url from "../../config/url";
import axios from "axios";
import FormItem from "../../components/FormItem";
import { withRouter } from "react-router-dom";
import Navigation from "../../components/Navigation";
import { Formik } from "formik";
import * as Yup from "yup";

const VerifikasiShape = Yup.object().shape({
  code: Yup.string().required("Kode wajib diisi")
});

export class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      initialValues: {
        code: ""
      },
      errorMessage: "",
      user: ""
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.resend = this.resend.bind(this);
  }

  componentDidMount() {
    let user = this.props.history.location.state?.user;
    if (!user) {
      let url = `/${this.props.match.params.username}/login`;
      this.props.history.push(url);
    }
  }

  async onSubmit(values, actions) {
    try {
      this.setState({
        errorMessage: null
      });
      values.user = this.props.history.location.state.user;
      let res = await axios.post(
        `${url}api/auth/forgot-password/verification`,
        values
      );

      if (res.data.code == "00") {
        message.success(res.data.info);
        let url = `/${this.props.match.params.username}/reset-password`;
        this.props.history.push(url, values);
      } else {
        this.setState({
          errorMessage: res.data.info
        });
      }
    } catch (err) {
      this.setState({
        errorMessage: "something went wrong"
      });
    }
  }

  async resend() {
    const hide = message.loading(
      "mengirim kode verifikasi, mohon tunggu...",
      0
    );
    try {
      let res = await axios.post(`${url}api/auth/forgot-password/request`, {
        user: this.state.user
      });
      //   console.log(res.data.code);
      if (res.data.code == "00") {
        message.success(res.data.info);
      } else {
        this.setState({
          errorMessage: res.data.info
        });
      }

      hide();
    } catch (error) {
      hide();
      this.setState({
        errorMessage: "something went wrong"
      });
    }
  }
  render() {
    return (
      <div className="w-full">
        <Navigation withBack noCart title="Lupa Password"></Navigation>
        <div className="flex justify-center items-center h-full flex-col mb-8">
          <div className="flex flex-col px-5 w-full md:w-1/3 lg:w-1/3 xl:w-1/3 pt-5">
            <div className="mx-3 md:mx-0">
              <div className="text-center mt-5">
                Masukan Kode Verifikasi yang telah kami kirimkan ke Email anda
              </div>
              <div className="text-center font-semibold mt-2 text-lg mb-5">
                {this.state.email}
              </div>
              {this.state.errorMessage && (
                <div className="bg-red-200 text-red-800 p-2 rounded-sm">
                  {this.state.errorMessage}
                </div>
              )}

              <Formik
                initialValues={this.state.initialValues}
                enableReinitialize
                validationSchema={VerifikasiShape}
                onSubmit={this.onSubmit}
              >
                {({
                  isSubmitting,
                  handleSubmit,
                  handleBlur,
                  handleChange,
                  errors,
                  touched,
                  values,
                  setFieldValue,
                  setFieldTouched
                }) => (
                  <Form onSubmit={handleSubmit}>
                    <FormItem
                      error={errors.code}
                      touched={touched.code}
                      label="Kode Verifikasi"
                      marginBottom="20px"
                    >
                      <Input
                        name="code"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.code}
                        placeholder="Kode verifikasi"
                      />
                    </FormItem>

                    <FormItem>
                      <Button
                        type="primary"
                        htmlType="submit"
                        block
                        loading={isSubmitting}
                      >
                        Submit
                      </Button>
                    </FormItem>
                    <div className="text-center">
                      Tidak Menerima Pesan Email?
                    </div>
                    <FormItem>
                      <button
                        type="button"
                        onClick={this.resend}
                        className="outline-none bg-white text-blue-700 w-full border-none cursor-pointer hover:underline"
                      >
                        Kirim Ulang Kode Verifikasi
                      </button>
                    </FormItem>
                  </Form>
                )}
              </Formik>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Login);
