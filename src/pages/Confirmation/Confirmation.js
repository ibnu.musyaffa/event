import React, { Component } from "react";
import FormItem from "../../components/FormItem";
import { withRouter } from "react-router-dom";
import { Form, Input, message, DatePicker, Icon, Spin } from "antd";
import Button from "../../components/Button";
import { Select } from "antd";
import { connect } from "react-redux";
import * as Yup from "yup";
import { Formik } from "formik";
import axios from "axios";
import url from "../../config/url";
import NavigationLight from "../../components/NavigationLight";
import { Helmet } from "react-helmet";
import number from "../../helper/format";

const { Option } = Select;

const confirmationShape = Yup.object().shape({
  payment_method_id: Yup.string().required("Tujuan transfer wajib diisi"),
  tanggal_transfer: Yup.string().required("Tanggal wajib diisi"),
  jumlah_transfer: Yup.string().required("Jumlah transfer wajib diisi"),
});

export class Confirmation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      initialValues: {
        payment_method_id: "",
        tanggal_transfer: "",
        jumlah_transfer: "",
        jumlah_transfer2: "",
      },
      bankList: [],
      status: "fetching",
      invoice: null,
      filePreview: null,
      file: null,
      transactionStatus: null,
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.fetchPaymentMethod();
    this.fetchTransaction();
  }

  handleChange(event) {
    this.setState({
      filePreview: URL.createObjectURL(event.target.files[0]),
      file: event.target.files[0],
    });
  }

  async onSubmit(values, actions) {
    let id = this.props.match.params.id;
    let token = this.props.match.params.token;

    try {
      if (!this.state.file) {
        message.error("Bukti Transfer belum di isi");
        return false;
      }
      let data = new FormData();
      data.set("username", this.props.match.params.username);
      data.set("payment_method_id", values.payment_method_id);
      data.set("tanggal_transfer", values.tanggal_transfer);
      data.set("jumlah_transfer", values.jumlah_transfer.replace(/\./g, ""));
      data.append("bukti_transfer", this.state.file);

      let response = await axios.post(
        `${url}api/event/transaction/bukti-transfer/${this.props.match.params.id}`,
        data,
        {
          headers: { "Content-Type": "multipart/form-data" },
        }
      );

      if (response.data.data.status_pembayaran == "CONFIRMED") {
        this.props.history.push(
          `/${this.props.match.params.username}/transaction-detail/${id}/${token}`
        );
        return;
      }

      this.setState({
        transactionStatus: response.data.data.status,
      });
    } catch (error) {
      if (error.response.data.message) {
        message.error(error.response.data.message);
        if (error.response.data.status_pembayaran == "CONFIRMED") {
          this.props.history.push(
            `/${this.props.match.params.username}/transaction-detail/${id}/${token}`
          );
        }
      } else {
        message.error("Terjadi Kesalahan");
      }
    }
  }

  async fetchPaymentMethod() {
    try {
      let username = this.props.match.params.username;
      let response = await axios.get(`${url}api/payment-method/${username}`);

      this.setState({
        bankList: response.data.data,
      });
    } catch (error) {
      message.error("Terjadi Kesalahan");
    }
  }

  async fetchTransaction() {
    try {
      let username = this.props.match.params.username;
      let id = this.props.match.params.id;
      let token = this.props.match.params.token;
      let response = await axios.get(
        `${url}api/event/transaction/${id}/${token}`
      );

      let status = response.data.data.status_pembayaran;

      if (status == "PAID" || status == "CONFIRMED") {
        this.props.history.push(
          `/${username}/transaction-detail/${id}/${token}`
        );
        return;
      }

      if (status == "EXPIRED") {
        this.props.history.push(`/${username}/checkout-detail/${id}/${token}`);
        return;
      }

      this.setState({
        status: "fetched",
        invoice: response.data.data.invoice,
        transactionStatus: response.data.data.status,
      });
    } catch (error) {
      if (error.response.data.message) {
        message.error(error.response.data.message);
      } else {
        message.error("Terjadi Kesalahan");
      }
    }
  }

  render() {
    if (this.state.status == "fetching") {
      return (
        <div>
          <div className="flex justify-center items-center h-64">
            <Spin size="large" />
          </div>
        </div>
      );
    }

    return (
      <div>
        <Helmet>
          <title>
            Konfirmasi Pembayaran - {this.props.match.params.username}
          </title>
        </Helmet>
        <NavigationLight
                    onClick={() => this.props.history.goBack()}
                    title="Konfirmasi Pembayaran"
                  ></NavigationLight>
        <Formik
          initialValues={this.state.initialValues}
          enableReinitialize={true}
          validationSchema={confirmationShape}
          onSubmit={this.onSubmit}
        >
          {({
            isSubmitting,
            handleSubmit,
            handleBlur,
            handleChange,
            errors,
            touched,
            values,
            setFieldValue,
            setFieldTouched,
          }) => (
            <Form onSubmit={handleSubmit}>
              <div className="m-5 flex justify-center relative">
                <div className="w-full sm:w-2/3 md:w-2/5 lg:w-1/3">
            
                  <div>
                    {/* {this.state.transactionStatus == "UNPAID" && (
                        <> */}
                    <FormItem label="Nomor Order" marginBottom="0.6rem">
                      <Input
                        value={this.state.invoice}
                        name="nomor_order"
                        size="large"
                        disabled
                        placeholder="Nomor Order"
                      />
                    </FormItem>
                    <FormItem
                      required
                      label="Tujuan Transfer"
                      error={errors.payment_method_id}
                      touched={touched.payment_method_id}
                      marginBottom="0.6rem"
                    >
                      <Select
                        name="payment_method_id"
                        size="large"
                        placeholder="Tujuan Transfer"
                        value={values.payment_method_id}
                        onBlur={() => setFieldTouched("payment_method_id")}
                        onChange={(value) => {
                          setFieldValue("payment_method_id", value);
                        }}
                        filterOption={false}
                      >
                        <Option key={0} value="">
                          Transfer Ke
                        </Option>
                        {this.state.bankList.map((item) => (
                          <Option key={item.id} value={item.id}>
                            {item.type.nama} - {item.nama_rekening} -{" "}
                            {item.nomor_rekening}
                          </Option>
                        ))}
                      </Select>
                    </FormItem>
                    <FormItem
                      required
                      label="Tanggal Transfer"
                      error={errors.tanggal_transfer}
                      touched={touched.tanggal_transfer}
                      marginBottom="0.6rem"
                    >
                      <DatePicker
                        onChange={(moment, dateString) =>
                          setFieldValue("tanggal_transfer", dateString)
                        }
                        className="w-full"
                        placeholder="Pilih Tanggal"
                        size="large"
                      />
                    </FormItem>
                    <FormItem
                      required
                      label="Jumlah transfer"
                      error={errors.jumlah_transfer}
                      touched={touched.jumlah_transfer}
                    >
                      <Input
                        name="jumlah_transfer"
                        onChange={(event) => {
                          // let newValue = event.target.value.replace
                          console.log(
                            event.target.value
                              .replace(/\D/, "")
                              .replace(".", "")
                          );
                          setFieldValue(
                            "jumlah_transfer",
                            event.target.value
                              .replace(/\D/, "")
                              .replace(/\./g, "")
                          );
                        }}
                        onBlur={handleBlur}
                        value={number(values.jumlah_transfer)}
                        size="large"
                        placeholder="Jumlah Transfer"
                      />
                    </FormItem>
                    {/* </>
                      )} */}
                  </div>

                  <div className="mt-8 mb-5">
                    <label
                      htmlFor="file-upload"
                      style={{ minHeight: 300 }}
                      className="hover:bg-gray-300 hover:text-gray-900 text-gray-600 block  rounded-lg cursor-pointer relative border border-gray-200 border-solid flex justify-center items-center"
                    >
                      <img
                        alt=""
                        className="rounded-lg max-h-full max-w-full object-fill"
                        src={this.state.filePreview}
                      />
                      <div
                        style={{ height: 70, width: 250 }}
                        className="bg-white absolute  font-bold text-sm rounded-lg flex justify-center items-center z-30 opacity-50 hover:opacity-75"
                      >
                        <Icon
                          type="camera"
                          style={{ fontSize: "2rem", marginRight: 10 }}
                          theme="filled"
                        />
                        {this.state.filePreview
                          ? "Ganti gambar"
                          : "Upload Bukti Transfer"}
                      </div>
                    </label>
                    <input
                      style={{ display: "none" }}
                      id="file-upload"
                      type="file"
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="mt-3">
                    <Button
                      loading={isSubmitting}
                      block
                      size="large"
                      type="primary"
                      htmlType="submit"
                    >
                      Submit
                    </Button>
                  </div>
                </div>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    );
  }
}

export default withRouter(Confirmation);
