import React, { Component } from "react";
import { Form, Icon, Input, Checkbox, message } from "antd";
import Button from "../../components/ButtonTeal";
import url from "../../config/url";
import axios from "axios";
import FormItem from "../../components/FormItem";
import { withRouter } from "react-router-dom";
import Navigation from "../../components/Navigation";
import { Formik } from "formik";
import * as Yup from "yup";

const ForgotPasswordShape = Yup.object().shape({
  user: Yup.string().required("Email wajib diisi")
  // .email("Email tidak valid")
});

export class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      initialValues: {
        user: ""
      },
      errorMessage: ""
    };

    this.onSubmit = this.onSubmit.bind(this);
  }

  async onSubmit(values, actions) {
    try {
      this.setState({
        errorMessage: null
      });

      let res = await axios.post(
        `${url}api/auth/forgot-password/request`,
        values
      );

      if (res.data.code == "00") {
        message.success(res.data.info);
        let url = `/${this.props.match.params.username}/forgot-password-verifikasi`;
        this.props.history.push(url, values);
      } else {
        this.setState({
          errorMessage: res.data.info
        });
      }
    } catch (err) {
      this.setState({
        errorMessage: "something went wrong"
      });
    }
  }

  render() {
    return (
      <div className="w-full">
        <Navigation withBack noCart title="Lupa Password"></Navigation>
        <div className="flex justify-center items-center h-full flex-col mb-8">
          <div className="flex flex-col px-5 w-full md:w-1/3 lg:w-1/3 xl:w-1/3 pt-5">
            <div className="mx-3 md:mx-0">
              {this.state.errorMessage && (
                <div className="bg-red-200 text-red-800 p-2 rounded-sm">
                  {this.state.errorMessage}
                </div>
              )}
              <Formik
                initialValues={this.state.initialValues}
                enableReinitialize
                validationSchema={ForgotPasswordShape}
                onSubmit={this.onSubmit}
              >
                {({
                  isSubmitting,
                  handleSubmit,
                  handleBlur,
                  handleChange,
                  errors,
                  touched,
                  values,
                  setFieldValue,
                  setFieldTouched
                }) => (
                  <Form onSubmit={handleSubmit}>
                    <FormItem
                      error={errors.user}
                      touched={touched.user}
                      label="Nomor Handphone atau Email"
                      marginBottom="20px"
                    >
                      <Input
                        name="user"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.user}
                        placeholder="Nomor Handphone atau email"
                      />
                    </FormItem>

                    <FormItem>
                      <Button
                        type="primary"
                        htmlType="submit"
                        block
                        loading={isSubmitting}
                      >
                        Submit
                      </Button>
                    </FormItem>
                  </Form>
                )}
              </Formik>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Login);
