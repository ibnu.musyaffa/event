import React, { Component } from "react";
import { message, Spin, Icon } from "antd";
import { withRouter } from "react-router-dom";
import format from "../../helper/format";
import axios from "axios";
import url from "../../config/url";
import Qr from "qrcode.react";
import Button from "../../components/Button";
import { Helmet } from "react-helmet";
import {
  EmailShareButton,
  WhatsappShareButton,
  TelegramShareButton,
} from "react-share";
import { EmailIcon, WhatsappIcon, TelegramIcon } from "react-share";

function Ticket(props) {
  let ticket = props.ticket;
  let event = props.event;
  return (
    <div className="border-solid border-gray-400 mb-5 border p-4 flex flex-col">
      <div className="flex justify-center items-center mb-5 mt-2">
        <div>
          <Qr
            size={150}
            className="w-full h-full"
            value={ticket.pivot.booking_code}
          ></Qr>
          <div className="text-center mt-2 font-semibold text-lg">
            {ticket.pivot.booking_code}
          </div>
        </div>
      </div>
      <div>
        <div>
          <div className="font-semibold text-base">
            {" "}
            {event.nama_event} -{" "}
            <span className="text-sm lowercase">[ {ticket.nama_tiket} ]</span>
          </div>
        </div>
        <div className="flex mt-2">
          <div className="mr-3">
            <Icon className="text-lg" type="calendar" />
          </div>
          <div>
            {event.tanggal_start == event.tanggal_end ? (
              event.tanggal_start
            ) : (
              <React.Fragment>
                {event.tanggal_start} - {event.tanggal_end}
              </React.Fragment>
            )}
          </div>
        </div>
        <div className="flex mt-2">
          <div className="mr-3">
            <Icon className="text-lg" type="clock-circle" />
          </div>
          <div>
            {event.waktu_start} - {event.waktu_end} WIB
          </div>
        </div>
        <div className="flex mt-2">
          <div className="mr-3">
            <Icon className="text-lg" type="home" />
          </div>
          <div>
            {event.nama_tempat}
          </div>
        </div>
        <div className="flex mt-2">
          <div className="mr-3">
            <Icon className="text-lg" type="environment" />
          </div>

          <div>
            {event.alamat}{" "}
            {event.map_link && (
              <React.Fragment>
                {"   -   "}
                <a className="no-print" href={event.map_link} target="_blank"  rel="noopener noreferrer">
                  Lihat Peta
                </a>
              </React.Fragment>
            )}{" "}
          </div>
        </div>
      </div>
    </div>
  );
}
export class CheckoutDetail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      status: "fetching",
      transaction: {},
    };
  }

  componentDidMount() {
    this.fetchTransaction();
  }

  async fetchTransaction() {
    try {
      let username = this.props.match.params.username;
      let id = this.props.match.params.id;
      let token = this.props.match.params.token;
      let res = await axios.get(`${url}api/event/transaction/${id}/${token}`);
      let status = res.data.data.status;
      if (status == "UNPAID" || status == "EXPIRED") {
        this.props.history.push(`/${username}/checkout-detail/${id}`);
        return;
      }
      if (status == "CONFIRMED") {
        this.props.history.push(`/${username}/transaction-detail/${id}`);
        return;
      }
      console.log(res.data.data);
      this.setState({
        status: "fetched",
        transaction: res.data.data,
      });
    } catch (error) {}
  }

  render() {
    if (this.state.status == "fetching") {
      return (
        <div>
          <div className="flex justify-center items-center h-64">
            <Spin size="large" />
          </div>
        </div>
      );
    }
    let id = this.props.match.params.id;
    let token = this.props.match.params.token;
    let username = this.props.match.params.username;
    let tikets_url = `${url}event/${username}/tickets/${id}/${token}`;
    return (
      <div>
        <Helmet>
          <title>Detail Transaksi - {this.props.match.params.username}</title>
        </Helmet>

        <div className="m-5 flex justify-center">
          <div className="w-full sm:w-2/3 md:w-2/5 lg:w-1/3">
            {this.state.transaction.tickets.map((item) => (
              <Ticket
                key={item.id}
                ticket={item}
                event={this.state.transaction.event}
              ></Ticket>
            ))}
            <div className="no-print flex justify-center items-center my-4">
              <EmailShareButton url={tikets_url}>
                <EmailIcon size={32} round={true} />
              </EmailShareButton>
              <div className="mx-1"></div>
              <WhatsappShareButton url={tikets_url}>
                <WhatsappIcon size={32} round={true} />
              </WhatsappShareButton>
              <div className="mx-1"></div>
              <TelegramShareButton url={tikets_url}>
                <TelegramIcon size={32} round={true} />
              </TelegramShareButton>
            </div>
            <div className="no-print">
              <Button
                className="no-print"
                size="large"
                type="primary"
                block
                onClick={() => window.print()}
              >
                Print Tiket
              </Button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(CheckoutDetail);
