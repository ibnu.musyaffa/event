import React, { Component } from "react";
import { Icon } from "antd";
export class NavigationLight extends Component {
  render() {
    return (
      <div className="z-10  flex justify-center items-center border-b border-t-0 border-l-0 border-r-0 h-16 border-solid border-gray-300">
        <div className="flex justify-center items-center relative w-full md:w-1/3">
          {!this.props.disableBack && (
            <div
              style={{ marginTop: 2 }}
              onClick={this.props.onClick}
              className="flex   justify-center items-center absolute left-0 rounded-full pl-5 md:pl-0  cursor-pointer"
            >
              <Icon
                style={{ fontSize: "20px", marginRight: 7 }}
                type="arrow-left"
              />
              <div>Back</div>
            </div>
          )}

          <div className="text-lg">{this.props.title}</div>
        </div>
      </div>
    );
  }
}

export default NavigationLight;
