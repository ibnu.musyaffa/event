import React from "react";
import url from '../config/url'
export default function Avatar(props) {
  return (
    <div className="flex justify-center items-center">
      <img
        className="rounded-full"
        src={`${url}/${props.setting?.logo_url}`}
        style={{ width: 30, height: 30 }}
        alt=""
      />
      <div className="px-5 pt-1 text-gray-900 text-base md:text-lg lg:text-lg xl:text-lg">
        {props.setting?.nama_toko}
      </div>
    </div>
  );
}
