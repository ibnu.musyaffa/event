import React from "react";
import { Icon, Spin } from "antd";
const antIcon = (
  <Icon className="mr-2" type="loading" style={{ color: "#fff" }} spin />
);

function Button(props) {
  let isFull = props.block ? "w-full" : "";

  let bgColor = "text-blue-500 bg-white";

  if (props.type == "primary") {
    bgColor = "bg-orange-400 hover:bg-orange-500";
    if (props.loading) {
      bgColor = "bg-orange-300 cursor-not-allowed";
    }
  }

  if (props.disabled) {
    bgColor = "bg-orange-200 cursor-not-allowed";
  }

  return (
    <button
      disabled={props.disabled || props.loading}
      onClick={props.onClick}
      type={props.htmltype}
      className={`${isFull} ${bgColor} text-white text-sm font-semibold focus:outline-none border-none cursor-pointer  py-2 px-4 shadow-sm rounded`}
    >
      {props.icon && (
        <Icon className="mr-2" type={props.icon} style={{ fontSize: 20 }} />
      )}
      {props.loading && <Spin indicator={antIcon} />}

      <span>{props.children}</span>
    </button>
  );
}

export default Button;
