import React, { Component } from "react";
import { Icon } from "antd";
import url from "../config/url";
function EventSmall(props) {
  return (
    <div
      className="group border-solid border-gray-300 border rounded-sm  
            mb-5 cursor-pointer h-auto relative p-4 flex hover:bg-gray-100"
    >
      <div className="mr-3">
        <img
          style={{ height: 100 }}
          className="rounded"
          src={`${url}${props.event.image_cover_url}`}
        ></img>
      </div>
      <div>
        <div className="font-semibold">{props.event.nama_event}</div>
        <div className="border-b border-solid border-r-0 border-l-0 border-t-0 border-gray-400 my-2"></div>
        <div className="flex items-center">
          <div className="mr-3 mt-1">
            <Icon
              className="text-lg"
              style={{ color: "#1c9fad" }}
              type="clock-circle"
            />
          </div>

          <div className="text-xs">
            {props.event.tanggal_start} {props.event.waktu_start} -{" "}
            {props.event.tanggal_end} {props.event.waktu_end}
          </div>
        </div>
        <div className="flex ">
          <div className="mr-3 mt-1">
            <Icon
              className="text-lg"
              style={{ color: "#1c9fad" }}
              type="environment"
            />
          </div>

          <div className="text-xs">
            {props.event.alamat.length > 70 ? (
              <React.Fragment>
                {props.event.alamat.substring(0, 70)}
                ....
              </React.Fragment>
            ) : (
              props.event.alamat
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
export default EventSmall;
