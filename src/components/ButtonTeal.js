import React from "react";
import { Icon, Spin } from "antd";
const antIcon = (
  <Icon
    className="mr-2"
    type="loading"
    style={{ color: "#fff" }}
    spin
  />
);

function Button(props) {
  let isFull = props.block ? "w-full" : "";

  let bgColor = "text-blue-500 bg-white";

  if (props.type == "primary") {
    bgColor = "bg-teal-400 hover:bg-teal-500 text-white";
    if (props.loading) {
      bgColor = "bg-teal-300 text-white";
    }
  }

  if (props.disabled) {
    bgColor = "bg-grey-400 text-grey-800";
  }

  return (
    <button
      disabled={props.disabled || props.loading}
      onClick={props.onClick}
      className={`${isFull} ${bgColor} text-sm  focus:outline-none h-10 border-none cursor-pointer  rounded`}
    >
      {props.icon && (
        <Icon className="mr-2" type={props.icon} style={{ fontSize: 20 }} />
      )}
      {props.loading && <Spin indicator={antIcon} />}

      {props.children}
    </button>
  );
}

export default Button;
