import React from 'react';
import { Form } from 'antd';
function FormItem({ children, error, touched, marginBottom, ...other }) {
  let newMarginBottom = marginBottom ? marginBottom : 13;
  return (
    <Form.Item
      hasFeedback={error && touched}
      validateStatus={error && touched ? 'error' : null}
      help={error && touched ? error : null}
      // marginBottom={newMarginBottom}
      style={{ marginBottom: newMarginBottom  }}
      {...other}
    >
      {children}
    </Form.Item>
  );
}

export default FormItem;
