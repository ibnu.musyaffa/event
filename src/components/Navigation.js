import React, { Component } from "react";
import Headroom from "react-headroom";
import Avatar from "./Avatar";
import { withRouter } from "react-router-dom";

import { Icon, Badge } from "antd";
import { connect } from "react-redux";


export class Navigation extends Component {
  constructor(props) {
    super(props);

 
  }


  render() {
    return (
      <Headroom>
        <div className="w-full flex justify-center bg-white  h-16 border border-solid border-gray-300 border-t-0 border-r-0 border-l-0 ">
          <div className="flex justify-center items-center relative w-full md:w-1/3 lg:w-1/3 xl:w-1/3">
            {this.props.withBack && (
              <div
                className=" text-gray-900 hover:text-teal-500 flex ml-3 md:ml-0  justify-center items-center absolute left-0 rounded-full  cursor-pointer"
                onClick={() => this.props.history.goBack()}
              >
                <Icon
                  className="hover:text-teal-500"
                  style={{ fontSize: "20px", marginRight: 7 }}
                  type="arrow-left"
                />
                <div className="">Back</div>
              </div>
            )}
            {this.props.title && (
              <div className="text-lg">{this.props.title}</div>
            )}

          
          </div>
        </div>
      </Headroom>
    );
  }
}



export default connect(
  null,
  null
)(withRouter(Navigation));
