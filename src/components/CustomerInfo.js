import React, { Component } from "react";

function Row(props) {
  return (
    <div className="mb-2">
      <div className="text-gray-600">{props.label}</div>
      <div className="font-semibold">{props.value}</div>
    </div>
  );
}

function CustomerInfo(props) {
  let expedisi = props.transaction?.expedisi?.split("+");

  return (
    <div>
      <Row label="Nama" value={props.transaction.nama}></Row>
      <Row gray label="Handphone" value={props.transaction.handphone}></Row>
      <Row label="Email" value={props.transaction.email}></Row>
    </div>
  );
}

export default CustomerInfo;
