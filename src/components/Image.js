import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
export class Image extends Component {
  render() {
    return (
      <img
        src={this.props.url}
        alt=""
        className="cursor-pointer hover:opacity-75"
        style={{ margin: 1, width: '32.7%' }}
        onClick={this.props.onClick}
      />
    );
  }
}

export default withRouter(Image);
