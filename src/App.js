import React from "react";
import { Router, Switch, Redirect, Route } from "react-router-dom";
import { PersistGate } from "redux-persist/integration/react";

import { Provider } from "react-redux";
import { store } from "./store";
import history from "./helper/history";
import { persistor } from "./store/index";

import Home from "./pages/Home/Home";
import EventDetail from "./pages/EventDetail/EventDetail";
import Checkout from "./pages/Checkout/Checkout";
import CheckoutDetail from "./pages/CheckoutDetail/CheckoutDetail";
import Confirmation from "./pages/Confirmation/Confirmation";
import TransactionDetail from "./pages/TransactionDetail/TransactionDetail";
import Login from "./pages/Login/Login";
import Register from "./pages/Register/Register";
import RegisterVerifikasi from "./pages/RegisterVerifikasi/RegisterVerifikasi";
import Transactions from "./pages/Transactions/Transactions";
import Menu from "./pages/Menu/Menu";
import ProfileEdit from "./pages/ProfileEdit/ProfileEdit";
import ForgotPassword from "./pages/ForgotPassword/ForgotPassword";
import ForgotPasswordVerifikasi from "./pages/ForgotPasswordVerifikasi/ForgotPasswordVerifikasi";
import ResetPassword from "./pages/ResetPassword/ResetPassword";
import PasswordEdit from "./pages/PasswordEdit/PasswordEdit";
import Tickets from "./pages/Tickets/Tickets";

import OnlyUnauthenticated from "./helper/Unauthenticated";
import OnlyAuthenticated from "./helper/Authenticated";
import ScrollToTop from "./helper/ScrollToTop";
import RefreshUser from './RefreshUser';
function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Router history={history}>
          <ScrollToTop></ScrollToTop>
          <RefreshUser></RefreshUser>
          <Switch>
            <Route path="/" exact>
              <div className="flex justify-center items-center h-full h-screen w-full flex-col">
                <div className="font-semibold text-6xl">404</div>
                <div className="text-3xl">NOT FOUND</div>
              </div>
            </Route>
            <Route path="/:username" exact>
              <Home></Home>
            </Route>
            <Route path="/:username/detail/:id?">
              <EventDetail></EventDetail>
            </Route>
            <Route path="/:username/checkout">
              <Checkout></Checkout>
            </Route>
            <Route path="/:username/checkout-detail/:id/:token">
              <CheckoutDetail></CheckoutDetail>
            </Route>

            <Route path="/:username/konfirmasi-pembayaran/:id/:token">
              <Confirmation></Confirmation>
            </Route>

            <OnlyAuthenticated path="/:username/transactions">
              <Transactions></Transactions>
            </OnlyAuthenticated>

            <Route path="/:username/transaction-detail/:id/:token">
              <TransactionDetail></TransactionDetail>
            </Route>

            <Route path="/:username/tickets/:id/:token">
              <Tickets></Tickets>
            </Route>

            <OnlyUnauthenticated path="/:username/login">
              <Login></Login>
            </OnlyUnauthenticated>
            <OnlyUnauthenticated path="/:username/register">
              <Register></Register>
            </OnlyUnauthenticated>
            <OnlyUnauthenticated path="/:username/register-verifikasi">
              <RegisterVerifikasi></RegisterVerifikasi>
            </OnlyUnauthenticated>

            <OnlyAuthenticated path="/:username/menu">
              <Menu></Menu>
            </OnlyAuthenticated>

            <OnlyAuthenticated path="/:username/edit-profile">
              <ProfileEdit></ProfileEdit>
            </OnlyAuthenticated>

            <OnlyAuthenticated path="/:username/edit-password">
              <PasswordEdit></PasswordEdit>
            </OnlyAuthenticated>

            <OnlyUnauthenticated path="/:username/forgot-password">
              <ForgotPassword></ForgotPassword>
            </OnlyUnauthenticated>
            <OnlyUnauthenticated path="/:username/forgot-password-verifikasi">
              <ForgotPasswordVerifikasi></ForgotPasswordVerifikasi>
            </OnlyUnauthenticated>
            <OnlyUnauthenticated path="/:username/reset-password">
              <ResetPassword></ResetPassword>
            </OnlyUnauthenticated>
          </Switch>
        </Router>
      </PersistGate>
    </Provider>
  );
}

export default App;
