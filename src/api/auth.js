import axios from "axios";
import url from "../config/url";
class Auth {
  static login(values) {
    return axios.post(`${url}api/auth/login`, {
      user: values.user,
      password: values.password
    });
  }
}


export default Auth;