import axios from "axios";
import url from "../config/url";
import createHeaders from "../helper/headers";

class Transaction {
  static index(params) {
    let headers = createHeaders();
    return axios.get(`${url}api/transaction`, {
      headers,
      params
    });
  }
}

export default Transaction;
