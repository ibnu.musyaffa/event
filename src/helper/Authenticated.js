import React from "react";

import { Route, Redirect } from "react-router-dom";
import isAuthenticated from "./isAuthenticated";

function Authenticated({ children, ...rest }) {
 
  let username = rest.computedMatch.params.username;
  return (
    <Route
      {...rest}
      render={({ location }) =>
        isAuthenticated() ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: `/${username}/login`,
              state: { from: location }
            }}
          />
        )
      }
    />
  );
}

export default Authenticated;
