import React from "react";

import { Route, Redirect } from "react-router-dom";
import isAuthenticated from "./isAuthenticated";
function Unauthenticated({ children, ...rest }) {
  let username = rest.computedMatch.params.username;
  return (
    <Route
      {...rest}
      render={({ location }) =>
        !isAuthenticated() ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: `/${username}`,
              state: { from: location }
            }}
          />
        )
      }
    />
  );
}

export default Unauthenticated;
