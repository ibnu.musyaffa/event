import React, { Component } from "react";
import createHeaders from "./helper/headers";
import axios from "axios";
import url from "./config/url";
import { connect } from "react-redux";
import { setUser } from "./store/user";
export class RefreshUser extends Component {
  async componentDidMount() {
    try {
      let [profile] = await Promise.all([this.fetchProfile()]);

      let user = profile.data.data?.user;
      console.log(user);
      this.props.setUser(user);
    } catch (error) {}
  }

  async fetchProfile() {
    let headers = createHeaders();
    let res = await axios.get(`${url}api/profile/custom`, {
      headers,
    });

    return res;
  }

  render() {
    return <div></div>;
  }
}

const mapDispatchToProps = (dispatch) => ({
  setUser: (user) => dispatch(setUser(user)),
});

export default connect(null, mapDispatchToProps)(RefreshUser);
